package omnitech.com.lgevents

import khronos.hours
import khronos.minus
import khronos.plus
import khronos.seconds
import omnitech.com.lgevents.transport.Synchronizer
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Example local unit test, which will execute on the development machine (host).

 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
class ExampleUnitTest {
    @Test
    @Throws(Exception::class)
    fun addition_isCorrect() {
        assertEquals(4, (2 + 2).toLong())
    }

    @Test
    fun test_Dates_Are_Compared() {
        assertTrue(!Synchronizer.shouldRun(Date(), 2, TimeUnit.HOURS))

        assertTrue(Synchronizer.shouldRun(Date().plus(2.hours), 2, TimeUnit.HOURS))

        assertTrue(Synchronizer.shouldRun(Date().minus(2.hours), 2, TimeUnit.HOURS))

        assertTrue(!Synchronizer.shouldRun(Date().minus(1.hours), 2, TimeUnit.HOURS))


        assertTrue(!Synchronizer.shouldRun(Date(), 2, TimeUnit.SECONDS))

        assertTrue(Synchronizer.shouldRun(Date().plus(2.seconds), 2, TimeUnit.SECONDS))

        assertTrue(Synchronizer.shouldRun(Date().minus(2.seconds), 2, TimeUnit.SECONDS))


        assertTrue(!Synchronizer.shouldRun(Date().minus(1.seconds), 2, TimeUnit.SECONDS))


    }
}