package omnitech.com.lgevents.database

import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.kotlinextensions.*
import com.raizlabs.android.dbflow.sql.language.OrderBy
import com.raizlabs.android.dbflow.sql.language.SQLOperator
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import kotlin.reflect.KClass

/**
 * Created by user on 5/31/2017.
 */
class ModelHelper {
    companion object {
        @JvmStatic
        val FLAG_ASCENDING = true
        @JvmStatic
        val FLAG_DESCENDING = false

        fun <T : Any> deleteAll(c: KClass<T>) {
            delete(c).execute()
        }


        fun <T : Any> findAllAsync(c: KClass<T>, orderBys: List<OrderBy> = listOf(), condition: SQLOperator? = null, callBack: (List<T>) -> Unit) {
            var select = (select from c).orderByAll(orderBys)

            if (condition != null)
                select = select.and(condition)

            select.async.list { _, list -> callBack(list) }

        }

        fun <T : Any> findAll(c: KClass<T>, condition: SQLOperator? = null): MutableList<T> {
            return (select from c).apply { condition?.let { where(condition) } }.list
        }

        fun <T : Any> findOneAsync(c: KClass<T>, sqlOperator: SQLOperator, callback: (T?) -> Unit) {
            (select from c where (sqlOperator)).limit(1)
                    .async
                    .list { _, list ->
                        if (list.isEmpty()) callback(null) else callback(list[0])
                    }
        }

        fun <T : Any> findOne(c: KClass<T>, sqlOperator: SQLOperator): T? {
            val data = (select from c where (sqlOperator)).limit(1).list
            return if (data.isEmpty()) return null else data[0]
        }

        fun <T : Any> listAll(c: KClass<T>, offset: Int = 0, max: Int = 0): List<T> {
            return SQLite.select().from(c).offset(offset).limit(max).list
        }

        fun <T : Any> deleteAll(l: List<T>) {
            l.forEach {
                val modelAdapter = FlowManager.getModelAdapter(it.javaClass)
                modelAdapter.delete(it)
            }
        }

    }
}

