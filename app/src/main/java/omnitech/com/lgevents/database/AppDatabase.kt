package omnitech.com.lgevents.database

import com.raizlabs.android.dbflow.annotation.Database
import com.raizlabs.android.dbflow.annotation.Migration
import com.raizlabs.android.dbflow.sql.SQLiteType
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration
import com.raizlabs.android.dbflow.sql.migration.BaseMigration
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper
import omnitech.com.lgevents.model.*
import java.util.*

/**
 * Created by kay
 */
@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
object AppDatabase {
    const val NAME: String = "LG_CHP"
    const val VERSION: Int = 12
}

@Migration(version = 2, priority = 1, database = AppDatabase::class)
class CreateProductCategoryFKMigration : AlterTableMigration<Product>(Product::class.java) {


    override fun onPreMigrate() {
        addForeignKeyColumn(SQLiteType.INTEGER, "category_id", "ProductCategory(`id`)")
    }

}

@Migration(version = 3, priority = 2, database = AppDatabase::class)
class InsertDummyProductGroupForAll : BaseMigration() {
    override fun migrate(database: DatabaseWrapper) {
        SQLite.insert(ProductCategory::class.java)
                .columns(
                        ProductCategory_Table.id,
                        ProductCategory_Table.name,
                        ProductCategory_Table.description,
                        ProductCategory_Table.uuid)
                .values(1, "Common", "Random Products", UUID.randomUUID().toString())
                .execute(database)

        SQLite.update(Product::class.java)
                .set(Product_Table.category_id.eq(1))
                .execute(database)
    }

}

@Migration(version = 4, priority = 1, database = AppDatabase::class)
class Add_Product_PackagingSizeMigration : AlterTableMigration<Product>(Product::class.java) {
    override fun onPreMigrate() {
        addColumn(SQLiteType.TEXT, "packagingSize")
    }
}

@Migration(version = 5, priority = 1, database = AppDatabase::class)
class Add_Product_WholeSalePrice_Migration : AlterTableMigration<Product>(Product::class.java) {
    override fun onPreMigrate() {
        addColumn(SQLiteType.REAL, "wholeSalePrice")
    }
}

@Migration(version = 6, priority = 1, database = AppDatabase::class)
class Add_Promotion_BuyingPrice_Migration : AlterTableMigration<Promotion>(Promotion::class.java) {
    override fun onPreMigrate() {
        addColumn(SQLiteType.REAL, "buyingPrice")
        addColumn(SQLiteType.REAL, "description")
    }
}


@Migration(version = 8, priority = 1, database = AppDatabase::class)
class Add_Product_VideoUrl : AlterTableMigration<Product>(Product::class.java) {
    override fun onPreMigrate() {
        addColumn(SQLiteType.TEXT, "videoUrl")
    }
}

@Migration(version = 12, priority = 1, database = AppDatabase::class)
class Add_Event_Time_To_Playback : AlterTableMigration<VideoPlayback>(VideoPlayback::class.java) {
    override fun onPreMigrate() {
        addColumn(SQLiteType.TEXT, "eventTime")
    }
}