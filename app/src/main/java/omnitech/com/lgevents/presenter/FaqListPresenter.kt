package omnitech.com.lgevents.presenter

import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.model.FAQ
import omnitech.com.lgevents.model.ViewFaQ
import omnitech.com.lgevents.view.FAQListActivity

/**
 * Created by user on 6/14/2017.
 */
class FaqListPresenter(val mActivity: FAQListActivity) {

    fun init(): FaqListPresenter {
        mActivity.onMenuCreated = {
            refresh(submitSearch = true)
        }

        mActivity.onItemExpanded = { faq ->
            Globals.lgPool.execute {
                ViewFaQ(faqQuestion = faq.fc.question,
                        faqUUID = faq.fc.uuid,
                        categoryName = faq.fc.catergory?.name ?: "n/a",
                        categoryUUID = faq.fc.catergory?.uuid ?: "n/a").saveEvent()
            }
        }
        return this
    }

    private fun refresh(submitSearch: Boolean = false) {
        val categoryId = mActivity.intent.getLongExtra("faq_category_id", -1)
        val searchQuery = mActivity.intent.getStringExtra("faq_search_query")
        mActivity.setTitle(mActivity.intent.getStringExtra("faq_category_name"))

        FAQ.findAllByCategoryId(categoryId, null) {
            //never send the search query so that all faqs are loaded into
            // the activity to make search more predictive
            mActivity.showQuestions(it)

            if (submitSearch && !searchQuery.isNullOrBlank()) {
                mActivity.mSearchView.isIconified = false
                mActivity.mSearchView.setQuery(searchQuery, true)
            }

        }
    }

}