package omnitech.com.lgevents.presenter

import com.dewarder.akommons.defaultSharedPreferences
import com.dewarder.akommons.showLongToast
import com.dewarder.akommons.startActivity
import kotlinx.android.synthetic.main.main_activity.*
import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.R
import omnitech.com.lgevents.transport.Synchronizer
import omnitech.com.lgevents.utils.str
import omnitech.com.lgevents.view.*

class MainMenuPresenter(val activity: MainMenuActivity) {

    fun init(): MainMenuPresenter {

        activity.btnUpcomingEvents.setOnClickListener {
            activity.startActivity<EventListActivity>()
        }


        activity.btnProductList.setOnClickListener {
            activity.startActivity<ProductListActivity>()
        }

        activity.btn_faq_list.setOnClickListener {
            activity.startActivity<FAQCategoryListActivity>()
        }

        activity.btn_resources_list.setOnClickListener{
            activity.startActivity<ResourcesListActivity>()
        }

        return this
    }

    fun syncAll() {
        if (activity.defaultSharedPreferences.getString(Globals.KEY_PHONE_NUMBER, null).isNullOrBlank()) {
            activity.showLongToast(activity.str(R.string.please_set_username))
        } else {
            Globals.lgPool.execute {
                Synchronizer(Globals.context, true).syncAll()

            }
        }


    }


}