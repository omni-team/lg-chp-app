package omnitech.com.lgevents.presenter

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.view.View
import com.dewarder.akommons.intentFor
import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.database.DummyData
import omnitech.com.lgevents.model.Resource
import omnitech.com.lgevents.model.ViewVideo
import omnitech.com.lgevents.transport.createRandomVideoFileFromUrl
import omnitech.com.lgevents.transport.downloadVideoFromUrl
import omnitech.com.lgevents.transport.isDownloading
import omnitech.com.lgevents.utils.doSafely
import omnitech.com.lgevents.utils.e
import omnitech.com.lgevents.utils.shortToast
import omnitech.com.lgevents.view.ResourcesListActivity
import omnitech.com.lgevents.view.VideoActivity
import android.view.Gravity
import android.widget.Toast
import android.content.Intent
import android.content.BroadcastReceiver
import omnitech.com.lgevents.transport.downloadStatus


class ResourcesListPresenter(val activity: ResourcesListActivity) {
    fun init(): ResourcesListPresenter {
        refresh()
//        insertDummyData()
        activity.onMenuCreated = {
            bindUi()
        }
        activity.onItemClicked = { v -> onItemClicked(v) }
        return this
    }

    private fun Context.playVideoWithEvents(url: String, title: String) {
        val videoFile = createRandomVideoFileFromUrl(url)
        val uri = Uri.fromFile(videoFile)
        if (videoFile.exists()) {
//        play
        } else {

        }
    }

    private fun onItemClicked(v: View) {
        val item = activity.recyclerView.itemAt<ResourcesListActivity.ResourcesCardView>(v)
        val videoFile = createRandomVideoFileFromUrl(item.resource.resourceUrl)
        val uri = Uri.fromFile(videoFile)
        if (videoFile.exists()) {
//        log here
            val intent = activity.intentFor<VideoActivity> {
                putExtra("resource_id", item.resource.id)
                putExtra("resource_title", item.resource.title)
                putExtra("resource_description", item.resource.description)
                putExtra("video_file",videoFile.absolutePath)
            }
            activity.startActivity(intent)
            Globals.lgPool.execute {
                item.resource?.let {
                    ViewVideo(videoTitle = it.title,
                            videoUUID = it.uuid).saveEvent()
                }
            }
        } else {
            activity.downloadStatus(uri.toString())
            if (activity.isDownloading(uri.toString())) {
                activity.shortToast("Video Is Already Downloading Please Wait!!")
            } else {
                doSafely("Downloading video ${item.resource.resourceUrl}") {
                    e("Resource presenter",item.resource.resourceUrl)
                    activity.downloadVideoFromUrl(item.resource.resourceUrl, item.resource.title, Environment.DIRECTORY_MOVIES, videoFile.name)
                }
            }
        }


    }

    fun refresh() {
        Resource.findAllNonExpiredVideos { activity.showVideos(it) }
    }

    private fun bindUi() {
        activity.onSearch = { query -> search(query) }
        activity.onCloseSearch = { refresh() }
    }

    fun search(query: String) {
        Resource.searchAll(query) {
            activity.showVideos(it)
        }
    }

    fun insertDummyData() {
        Resource.findAllNonExpiredVideos {
            if (it.isEmpty()) {
                DummyData.createDummyVideoResources()
                refresh()
            }
        }

    }
}