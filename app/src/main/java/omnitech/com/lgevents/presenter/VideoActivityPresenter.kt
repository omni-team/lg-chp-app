package omnitech.com.lgevents.presenter

import kotlinx.android.synthetic.main.video_activity.*
import omnitech.com.lgevents.model.Resource
import omnitech.com.lgevents.view.VideoActivity

class VideoActivityPresenter(val activity:VideoActivity){

    lateinit var video: Resource

    fun init():VideoActivityPresenter{
        refresh()
        return this
    }

    private fun refresh(){
        val title = activity.intent.getStringExtra("resource_title")
        val description = activity.intent.getStringExtra("resource_description")
        val id = activity.intent.getLongExtra("resource_id",0)
        val videoFilePath = activity.intent.getStringExtra("video_file")
        activity.setTitle(title)
        activity.video_show_title.text = title
        activity.video_show_description.text = description
        activity.video = Resource.get(id)!!
        activity.videoPath = videoFilePath
    }
}