package omnitech.com.lgevents.presenter

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.dewarder.akommons.startActivity
import com.udojava.evalex.Expression
import kotlinx.android.synthetic.main.product__saving__input_line.view.*
import kotlinx.android.synthetic.main.product__saving_output_line.view.*
import kotlinx.android.synthetic.main.product_show_activity.*
import omnitech.com.lgevents.R
import omnitech.com.lgevents.model.Product
import omnitech.com.lgevents.model.SavingCalculator
import omnitech.com.lgevents.transport.playVideo
import omnitech.com.lgevents.utils.dToast
import omnitech.com.lgevents.utils.format
import omnitech.com.lgevents.utils.longToast
import omnitech.com.lgevents.utils.str
import omnitech.com.lgevents.view.ImagesActivity
import omnitech.com.lgevents.view.MainMenuActivity
import omnitech.com.lgevents.view.ProductShowActivity
import java.math.BigDecimal

/**
 * Created by user on 5/23/2017.
 */
class ProductShowPresenter(val activity: ProductShowActivity) : TextWatcher {


    lateinit var mProduct: Product

    val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            setupVideo()
        }

    }

    fun init(): ProductShowPresenter {
        activity.imgProduct.setOnClickListener {
            //note: consider using PaperParcel
            activity.startActivity<ImagesActivity> {
                putExtra("url", mProduct.imageURl)
                putExtra("name", mProduct.name)
            }

        }

        val longExtra = activity.intent.getLongExtra("id", 0)
        Product.get(longExtra) {
            if (it != null) {
                mProduct = it
                activity.setProduct(it)
                mayBeShowVariables(mProduct)
                setupVideo()

            } else {
                activity.longToast(activity.str(R.string.products_have_updated))
                activity.finish()
                activity.startActivity<MainMenuActivity>()
            }

        }

        activity.registerReceiver(receiver, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))

        activity.fnOnDestroy = { activity.unregisterReceiver(receiver) }
        return this
    }

    private fun setupVideo() {
        if (mProduct.getVideoUrlOrDefault() == null) {
            activity.btn_download_video.visibility = View.GONE
        } else {

            val videoFileExists = mProduct.videoFileExists()

            if (videoFileExists) {
                activity.btn_download_video.text = activity.str(R.string.ic_play_video)
            } else {
                activity.btn_download_video.text = activity.str(R.string.ic_get_video)

            }

            activity.btn_download_video.setOnClickListener {
                val videoUrlOrDefault = mProduct.getVideoUrlOrDefault()
                if (videoUrlOrDefault != null) {
                    activity.playVideo(videoUrlOrDefault, "Video: ${mProduct.name}")
                }
            }
        }
    }


    fun mayBeShowVariables(p: Product) {

        if (p.savingCalculatorId == null) {
            activity.card_savings.visibility = View.GONE
            return
        }

        val scId = p.savingCalculatorId!!

        SavingCalculator.findByUUID(scId) { sc ->
            if (sc != null) {
                sc.getVariables { vars -> activity.showVariables(sc, vars) }
            } else {
                activity.card_savings.visibility = View.GONE
            }
        }

    }

    override fun afterTextChanged(s: Editable?) {

        val allHaveData = activity.inputViews.all { !it.value.edit_variable_input.text.isNullOrBlank() }

        if (!allHaveData) return

        val contextVariables = mutableMapOf<String, BigDecimal?>()

        activity.outPutViews.forEach { e ->
            val variable = e.key
            val view = e.value

            val expr = Expression(variable.formula)

            activity.inputViews.forEach { outputVar ->
                expr.setVariable(
                        outputVar.key.variableName,
                        BigDecimal(outputVar.value.edit_variable_input.text.toString()))
            }

            expr.setVariable("price", BigDecimal(mProduct.resolvePrice()))
            expr.setVariable("cost", BigDecimal(mProduct.resolvePrice()))

            contextVariables.forEach { cv -> expr.setVariable(cv.key, cv.value) }


            try {

                val output = expr.eval()

                view.txt_variable_output.text = output?.toDouble()?.format("###,###.##")

                contextVariables.put(variable.variableName, output)
            } catch (x: Exception) {

                activity.dToast("Error evaluating expression [${x.message}]")

                x.printStackTrace()
                view.txt_variable_output.text = activity.getString(R.string.error)
            }

        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}


}