package omnitech.com.lgevents.presenter

import omnitech.com.lgevents.model.Event
import omnitech.com.lgevents.view.EventListActivity

/**
 * Created by user on 6/20/2017.
 */
class EventListPresenter(val activity: EventListActivity) {

    fun init(): EventListPresenter {
        refresh()
        return this
    }

    fun refresh() {
        Event.findAllNonExpiredEvents { activity.showEvents(it) }
    }

}