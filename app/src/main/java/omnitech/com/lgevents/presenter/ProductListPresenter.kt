package omnitech.com.lgevents.presenter

import com.dewarder.akommons.intentFor
import kotlinx.android.synthetic.main.product__group_filter_view.*
import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.model.Product
import omnitech.com.lgevents.model.ProductCategory
import omnitech.com.lgevents.model.ViewProduct
import omnitech.com.lgevents.view.ProductListActivity
import omnitech.com.lgevents.view.ProductShowActivity

/**
 * Created by user on 5/20/2017.
 */
class ProductListPresenter(val activity: ProductListActivity) {
    fun init(): ProductListPresenter {
        activity.onMenuCreated = {
            bindUi()
            refresh()
        }
        return this
    }


    private fun bindUi() {

        activity.loTagsLayout.setOnTagClickListener { view, position, parent ->
            val category = activity.productCategories[position]
            activity.collapseFilterBar()
            filterCategory(category)
            true
        }

        activity.onSearch = { query -> search(query) }

        activity.onCloseSearch = { refresh() }
    }


    fun search(query: String) {
        if (query.isNullOrBlank()) {
            refresh()
        } else {
            Product.searchAll(query) {
                setCategoriesForProducts(it.map { it.category?.id }.filterNotNull())
                activity.setProducts(it)
            }
        }
    }

    fun setCategoriesForProducts(productIds: List<Long>) {
        ProductCategory.findAllByIds(productIds) { activity.setCategories(it) }
    }

    fun refresh() {
        loadAllProducts()
        loadCategories()
    }

    fun loadAllProducts() {
        Product.findAll { activity.setProducts(it) }
    }

    fun loadCategories() {
        ProductCategory.findAll { activity.setCategories(it) }
    }

    fun filterCategory(c: ProductCategory) {
        activity.txtGroupName.text = "${c.name} : ${c.description ?: ""}"
        val searchQuery = getSearchQuery()
        if (searchQuery.isNullOrBlank()) {
            if (c.id == -1L) {
                loadAllProducts()
            } else {
                Product.findAllByCategory(c) { activity.setProducts(it) }
            }
        } else {
            if (c.id == -1L) {
                search(searchQuery)
            } else {
                Product.searchAll(searchQuery, c.id) { activity.setProducts(it) }
            }
        }

    }

    private fun getSearchQuery() = activity.mSearchView.query.toString()


    fun onItemSelected(productId: Long) {
        val intent = activity.intentFor<ProductShowActivity> { putExtra("id", productId) }
        activity.startActivity(intent)

        Globals.lgPool.execute {
            Product.get(productId)?.let {
                ViewProduct(productUUID = it.uuid,
                            productName = it.name,
                            categoryName = it.category?.name ?: "n/a",
                            categoryUUID = it.category?.uuid ?: "n/a").saveEvent()
            }
        }
    }
}