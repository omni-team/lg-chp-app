package omnitech.com.lgevents.presenter

import android.view.View
import com.dewarder.akommons.startActivity
import kotlinx.android.synthetic.main.faq_category_list_activity.*
import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.model.FAQCategory
import omnitech.com.lgevents.model.ViewFaQCategory
import omnitech.com.lgevents.view.FAQCategoryListActivity
import omnitech.com.lgevents.view.FAQListActivity
import omnitech.com.lgevents.view.FaqCategoryCardView

/**
 * Created by user on 6/14/2017.
 */
class FAQCategoryListPresenter(val activity: FAQCategoryListActivity) {

    fun init(): FAQCategoryListPresenter {
        refresh()
        activity.onMenuCreated = {
            bindUi()
        }

        activity.onItemClicked = { v -> onItemClicked(v) }
        return this

    }

    private fun onItemClicked(v: View) {
        val item = activity.rcyc_faq_category_list.itemAt<FaqCategoryCardView>(v)

        Globals.lgPool.execute { ViewFaQCategory(item.fc.name, item.fc.uuid).saveEvent() }

        activity.startActivity<FAQListActivity> {
            putExtra("faq_category_id", item.fc.id)
            putExtra("faq_category_name", item.fc.name)
            putExtra("faq_search_query", activity.mSearchView.query.toString())
        }
    }

    private fun bindUi() {
        activity.onSearch = { query -> search(query) }

        activity.onCloseSearch = { refresh() }
    }

    fun search(query: String) {
        FAQCategory.searchAll(query) { activity.showCategories(it) }
    }


    fun refresh() {
        FAQCategory.findAll { activity.showCategories(it) }
    }

}