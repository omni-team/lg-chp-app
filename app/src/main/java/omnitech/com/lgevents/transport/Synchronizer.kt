package omnitech.com.lgevents.transport

import android.annotation.SuppressLint
import android.content.Context
import com.bumptech.glide.request.FutureTarget
import com.dewarder.akommons.defaultSharedPreferences
import com.dewarder.akommons.isConnectedToMobile
import com.dewarder.akommons.isConnectedToWifi
import com.omnitech.lgprotobuf.*
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.kotlinextensions.databaseForTable
import com.raizlabs.android.dbflow.kotlinextensions.save
import khronos.Dates
import khronos.toString
import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.database.ModelHelper
import omnitech.com.lgevents.job.EventNotificationJob
import omnitech.com.lgevents.model.*
import omnitech.com.lgevents.utils.*
import org.humanizer.jvm.humanize
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantReadWriteLock

/**
 * Created by user on 6/11/2017.
 */
class Synchronizer(val ctx: Context, val manualTrigger: Boolean) {


    private val latch = LgCountDownLatch(5)


    companion object {
        private var syncCount = 0
        private val lock = ReentrantReadWriteLock()


        private fun doLock() {
            lock.writeLock().lock()
        }

        private fun doUnlock() {
            lock.writeLock().unlock()
        }

        fun shouldRun(thenDate: Date, sleepDuration: Long, tu: TimeUnit): Boolean {
            val now = System.currentTimeMillis()
            val then = thenDate.time

            val diff = now - then
            if (diff < 0) return true

            var finalSleepTime = sleepDuration
            if (tu != TimeUnit.MILLISECONDS) {
                finalSleepTime = tu.toMillis(sleepDuration)
            }

            if (diff >= finalSleepTime) return true

            return false
        }

    }

    fun syncAll() {

        if (ctx.isConnectedToWifi || ctx.isConnectedToMobile) {

            try {
                doLock()
                val shouldRun = shouldRun()

                if (!shouldRun) {
                    ctx.dShortToast("*****SYNC RUN RECENTLY SYNCING ABORTING [${Synchronizer.syncCount}].TRY AGAIN IN 30 SECONDS*****")
                    return
                }

                setLastRun()

            } finally {
                doUnlock()

            }

            Synchronizer.syncCount++

            mayBeShowMsg("Sync Started...")

            ctx.doSafelyDebug("Picking User Info") { authenticate() }

            ctx.doSafelyDebug("Syncing Products") { syncProducts() }

            ctx.doSafelyDebug("Syncing FAQs") { syncFaqs() }

            ctx.doSafelyDebug("Syncing Events") { syncEvents() }

            ctx.doSafelyDebug("Syncing Saving Calculator") { syncSavingCalculator() }

            ctx.doSafelyDebug("Syncing Resources") { syncResources() }

            ctx.doSafelyDebug("Uploading FAQ Logs") { uploadFaqLogs() }

            ctx.doSafelyDebug("Uploading FAQCategory Logs") {
                uploadFaqCategoryLogs()
            }

            ctx.doSafelyDebug("Uploading Product Logs") { uploadProductLogs() }

            ctx.doSafelyDebug("Uploading Video View Logs") {
                uploadViewVideoLogs()
            }

            ctx.doSafelyDebug("Uploading Video PlayBack Logs") { uploadVideoPlayBackLogs() }

            //Only wait for downloads to finish in manual trigger mode
            if (manualTrigger) {

                try {
                    latch.await(2, TimeUnit.MINUTES)
                    mayBeShowMsg("*****SYNC[${Synchronizer.syncCount}] COMPLETED*****")
                } catch (e: Exception) {
                    ctx.dShortToast("Unable to wait for sync completion.[${e.message}]")
                    //ignore
                }
            }

        } else {
            mayBeShowMsg("No Network Available. Cannot Sync Items")
        }

    }


    fun authenticate() {
        Transport(ctx, latch, manualTrigger).apply {

            request = clientRequest(ClientRequest.RequestType.AUTHENTICATE, username())

            onSuccess = { result ->

                mayBeShowMsg("Authenticated USER is : [${result.human_name}]")

                val editor = ctx.defaultSharedPreferences.edit()
                editor.putString(Globals.KEY_USERNAME, result.username)
                editor.putString(Globals.KEY_USERID, result.user_id)
                editor.putString(Globals.KEY_HUMAN_NAME, result.human_name)
                editor.apply()


            }
        }.execute()
    }

    fun mayBeShowMsg(msg: String) {
        if (manualTrigger) {
            ctx.shortToast(msg)
        } else {
            ctx.dShortToast(msg)
        }
    }

    fun syncFaqs() {
        Transport(ctx, latch, manualTrigger).apply {
            request = clientRequest(ClientRequest.RequestType.FAQ_LIST, username())

            onSuccess = { result ->
                ctx.doSafelyDebug("Saving FAQs") { saveFAQs(result.faq_category_list) }

            }
        }.execute()
    }

    private fun saveFAQs(msgFaqCategoryList: MsgFaqCategoryList) {
        databaseForTable<FAQ>().executeTransaction {

            ModelHelper.deleteAll(FAQ::class)
            ModelHelper.deleteAll(FAQCategory::class)

            msgFaqCategoryList.categories.forEach { faqCategory -> saveFAQCategory(faqCategory) }

            mayBeShowMsg("Finished Downloading FAQs")
        }

    }

    private fun saveFAQCategory(msgFaqCategory: MsgFaqCategory) {
        if (msgFaqCategory.faqs == null || msgFaqCategory.faqs.isEmpty()) {
            return
        }

        val faqCategory = msgFaqCategory.toFAQCategory().apply { save() }
        val faqs = msgFaqCategory.faqs.map { it.toFAQ(faqCategory) }
        FAQ_Table(databaseForTable<FAQ>()).insertAll(faqs)
    }


    fun syncProducts() {


        Transport(ctx, latch, manualTrigger).apply {
            request = clientRequest(ClientRequest.RequestType.CATEGORY_LIST, username())

            onSuccess = {
                ctx.doSafelyDebug("Saving Products") { saveProducts(it.product_category_list) }
            }
        }.execute()
    }

    private fun saveProducts(productMsgs: MsgProductCategoryList) {

        databaseForTable<Product>().executeTransaction {
            Product.deleteAll()
            ProductCategory.deleteAll()

            productMsgs.categories.forEach { saveProductCategory(it) }

            mayBeShowMsg("Finished Downloading Products")

            ctx.doSafelyDebug("Fetching Images") { prefetchAllProductImages() }
        }

    }

    private fun saveProductCategory(categoryMsg: MsgProductCategory) {

        if (categoryMsg.products == null || categoryMsg.products.isEmpty()) {
            return
        }

        val productCategory = categoryMsg.toProductCategory().apply { save() }

        val products = categoryMsg.products.map {

            val p = if (it.promotion != null) {
                it.promotion.toPromotion().apply { save() }
            } else {
                null
            }
            it.toProduct(productCategory).apply { promotion = p }
        }

        Product_Table(databaseForTable<Product>()).insertAll(products)

    }

    private fun username(): String {
        return ctx.defaultSharedPreferences.getString("chp_username", "0778922055")
    }

    fun prefetchAllProductImages() {


        Product.findAll { products ->

            val futures = mutableListOf<FutureTarget<File?>>()

            products.forEach { product ->

                if (!product.imageURl.isNullOrBlank()) {
                    ctx.doSafelyDebug("Prefetch-ing Image [${product.imageURl}]") {
                        futures.add(ctx.preFetchImageGlide(product.imageURl!!))
                    }
                }
            }

            val notifier = FutureIsDoneNotifier<File?, FutureTarget<File?>>(
                    "PreFetching Images", ctx, futures)

            Thread(notifier).start()
        }

    }


    fun syncEvents() {

        Transport(ctx, latch, manualTrigger).apply {
            request = clientRequest(ClientRequest.RequestType.EVENT_LIST, username())

            onSuccess = { r ->
                FlowManager.getDatabaseForTable(Event::class.java).executeTransaction {
                    ctx.doSafelyDebug("Saving Events") { saveEvents(r.event_list) }
                }
            }
        }.execute()
    }

    private fun saveEvents(eventList: MsgEventList) {
        val events = eventList.events.map {
            ctx.doSafelyDebug("Convert Msg$it To Event") { it.toEvent() }
        }.filterNotNull()

        val modelAdapter = FlowManager.getModelAdapter(Event::class.java)
        Event.deleteAll()
        modelAdapter.insertAll(events)
        mayBeShowMsg("Finished Downloading Events [${events.size}]")

        EventNotificationJob(ctx).showEventsComingSoon().showEventsHappeningNow()

    }

    fun syncSavingCalculator() {
        Transport(ctx, latch, manualTrigger).apply {
            request = clientRequest(ClientRequest.RequestType.SAVING_CALCULATOR_LIST, username())

            onSuccess = { r ->
                ctx.doSafelyDebug("Saving SavingCalculators") {
                    FlowManager.getDatabaseForTable(SavingCalculator::class.java).executeTransaction {
                        savingCalculators(r.saving_calculator_list)
                    }
                }

            }
        }.execute()
    }

    fun savingCalculators(calculators: MsgSavingCalculatorList) {

        SavingCalculatorVariable.deleteAll()
        SavingCalculator.deleteAll()
        var saved = 0
        calculators.saving_calculator_list.map {
            ctx.doSafelyDebug("Converting Msg$it To SavingCalculator") {
                saved += saveSavingCalculator(it)
            }
        }

        mayBeShowMsg("Finished Downloading Calculators[${saved}]")


    }

    fun saveSavingCalculator(msgSc: MsgSavingCalculator): Int {

        if (msgSc.variables != null && msgSc.variables.isEmpty()) {
            ctx.dShortToast("Ignoring saving SavingCalc[${msgSc.name}] coz it has no variables")
            return 0
        }


        val sc = msgSc.toSavingCalculator().apply { save() }

        val variables = msgSc.variables.map { it.toSavingCalculatorVariable(sc) }
                .filter {
                    if (it.variableName != "price") {
                        true
                    } else {
                        ctx.dShortToast("Ignoring hardcoded Price Variable in SavingCalc[${msgSc.name}]")
                        false
                    }
                }

        val adapter = FlowManager.getModelAdapter(SavingCalculatorVariable::class.java)

        adapter.insertAll(variables)

        return 1


    }

    private fun syncResources(){
        Transport(ctx,latch,manualTrigger).apply {
            request = clientRequest(ClientRequest.RequestType.MEDIA_RESOURCE_LIST,username())

            onSuccess = {r->
                FlowManager.getDatabaseForTable(Resource::class.java).executeTransaction {
                    ctx.doSafelyDebug("Saving Resources..."){saveResources(r.resource_list)}
                }
            }
        }.execute()
    }

    private fun saveResources(resourceList: MsgMediaResourceList){
        val resources = resourceList.resources.map {
            ctx.doSafelyDebug("Convert Msg$it to Resource"){it.toResource()}
        }.filterNotNull()

        val modelAdapter = FlowManager.getModelAdapter(Resource::class.java)
        Resource.deleteAll()
        modelAdapter.insertAll(resources)
        mayBeShowMsg("Finished downloading Resources [${resources.size}]")
    }

    fun uploadProductLogs() {
        ItemUploader<ViewProduct>(ViewProduct::class,
                ctx,
                manualTrigger).apply {

            onConfigureRequest = { list ->
                clientRequest(ClientRequest.RequestType.UPLOAD_PRODUCT_LOG, username()) {
                    view_product_log_list(MsgViewProductLogList(list.map { it.toProtoMsg() })).build()
                }
            }

        }.upload()
    }

    fun uploadFaqLogs() {
        ItemUploader<ViewFaQ>(ViewFaQ::class,
                ctx,
                manualTrigger).apply {

            onConfigureRequest = { list ->
                clientRequest(ClientRequest.RequestType.UPLOAD_FAQ_LOG, username()) {
                    view_faq_log_list(MsgViewFaQLogList(list.map { it.toProtoMsg() })).build()
                }
            }

        }.upload()
    }

    fun uploadFaqCategoryLogs() {
        ItemUploader<ViewFaQCategory>(ViewFaQCategory::class,
                ctx,
                manualTrigger).apply {

            onConfigureRequest = { list ->
                clientRequest(ClientRequest.RequestType.UPLOAD_FAQ_CATEGORY_LOG, username()) {
                    view_faq_category_list(MsgViewFAQCategoryLogList(list.map { it.toProtoMsg() })).build()
                }
            }

        }.upload()
    }

    fun uploadViewVideoLogs() {
        ItemUploader<ViewVideo>(ViewVideo::class,
                ctx,
                manualTrigger).apply {
            onConfigureRequest = { list ->
                clientRequest(ClientRequest.RequestType.VIEW_VIDEO_LOG, username()) {
                    view_video_log_list(MsgViewVideoLogList(list.map { it.toProtoMsg() })).build()
                }
            }
        }.upload()
    }

    fun uploadVideoPlayBackLogs() {
        ItemUploader(VideoPlayback::class,
                ctx,
                manualTrigger).apply {
            onConfigureRequest = { list ->
                clientRequest(ClientRequest.RequestType.VIDEO_PLAY_BACK, username()) {
                    video_playback_list(MsgVideoPlaybackLogList(list.map { it.toProtoMsg() })).build()
                }
            }
        }.upload()
    }

    @SuppressLint("CommitPrefEdits")
    private fun setLastRun() {
        val editor = ctx.defaultSharedPreferences.edit()
        val nowStamp = Dates.now.toString(LG_TIME_STAMP_FORMAT)
        d("======Setting last Sync Time [$nowStamp]")
        editor.putString(Globals.KEY_SETTING_LAST_SYNC_ID, nowStamp)
        editor.commit()
    }

    private fun shouldRun(): Boolean {
        val dateStr = ctx.defaultSharedPreferences.getString(Globals.KEY_SETTING_LAST_SYNC_ID, Date(1).toLgFormat())
        val d: Date = dateStr.toDate(format = LG_TIME_STAMP_FORMAT)
        d("======Last Sync Time is: $dateStr[${d.humanize()}]")
        val sleepDuration = if (manualTrigger) Globals.SYNC_MANUAL_PAUSE_TIME else Globals.SYNC_BACKGROUND_FORCE_PAUSE_PERIOD
        return shouldRun(d, sleepDuration, Globals.SYNC_SLEEP_UNIT)
    }


}