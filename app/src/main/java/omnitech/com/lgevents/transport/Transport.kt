package omnitech.com.lgevents.transport

import android.content.Context
import com.dewarder.akommons.Permission
import com.dewarder.akommons.isPermissionsGranted
import com.omnitech.lgprotobuf.ClientRequest
import com.omnitech.lgprotobuf.ServerResponse
import okhttp3.*
import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.R
import omnitech.com.lgevents.utils.*
import org.humanizer.jvm.truncate
import java.io.IOException

/**
 * Created by user on 6/11/2017.
 */
class Transport(val ctx: Context, val countDownLatch: Latch, val manualTrigger: Boolean = false) {

    var onError: (String) -> Unit = {
        if (manualTrigger && !ctx.isDevMode()) {
            ctx.shortToast("Error Syncing: ${request.request}. Please Check Your Network")
        } else {
            ctx.dToast(it)
        }
    }
    lateinit var onSuccess: (ServerResponse) -> Unit
    lateinit var request: ClientRequest


    fun execute() {
        ctx.doSafelyDebug("Downloading ${request.request}") {
            try {
                executeImpl()
            } catch (x: Exception) {
                countDownLatch.countDown()
            }
        }
    }

    private fun executeImpl() {
        if (!ctx.isPermissionsGranted(Permission.INTERNET)) {
            onError.invoke("No Network Available To Synchronize Items!!")
            countDownLatch.countDown()
            return
        }

        val url = ctx.str(R.string.default_server_url)

        val requestBody = RequestBody.create(MediaType.parse("application/binary"), request.encode())

        val r = Request.Builder()
                .url(url)
                .post(requestBody)
                .build()

        val call = Globals.httpClient.newCall(r)
        call.enqueue(object : Callback {
            override fun onFailure(errorCall: Call?, e: IOException?) {
                countDownLatch.countDown()
                onError.invoke("Error while making request[$request]. ${e?.message}")
            }

            override fun onResponse(sourceCall: Call?, response: Response?) {
                countDownLatch.countDown()
                if (response!!.isSuccessful) {
                    try {

                        decodeResponse(response)
                    } catch (x: Exception) {
                        x.printStackTrace()
                        onError.invoke("Error Decoding Reponse for ${request.toString().truncate(100)} : ${x.message}")
                    }
                } else {
                    onError.invoke("Error while making request[$request]. ${response.message()}")
                }
            }
        })
    }

    private fun decodeResponse(response: Response) {
        val body = response.body()


        if (body != null) {
            val serverRepose = ServerResponse.ADAPTER.decode(body.byteStream())
            if (serverRepose.isSuccessful()) {
                onSuccess.invoke(serverRepose)
            } else {
                onError.invoke("ServerError(${request.request}): ${serverRepose.message}")
            }
        } else {
            onError.invoke("Error while Downloading ${request.request}")
        }
    }


}