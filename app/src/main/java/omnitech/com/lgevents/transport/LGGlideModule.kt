package omnitech.com.lgevents.transport

/**
 * Created by user on 6/27/2017.
 */
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class LGGlideModule : AppGlideModule()