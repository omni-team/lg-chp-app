package omnitech.com.lgevents.transport

import android.content.Context
import com.omnitech.lgprotobuf.ClientRequest
import omnitech.com.lgevents.database.ModelHelper
import omnitech.com.lgevents.utils.Latch
import omnitech.com.lgevents.utils.dShortToast
import kotlin.reflect.KClass


class ItemUploader<T : Any>(
        val c: KClass<T>,
        val context: Context,
        val manualTrigger: Boolean,
        val pageSize: Int = 20) {

    lateinit var onConfigureRequest: (List<T>) -> ClientRequest


    fun upload() {
        context.dShortToast("Uploading ${c.java.simpleName}")
        doUpload()
    }

    private fun doUpload() {
        val items = ModelHelper.listAll(c, max = pageSize)

        if (items.isNotEmpty()) {
            upload(items, true)
        }
    }

    private fun upload(items: List<T>, retry: Boolean) {
        Transport(context, Latch.DEFAULT, manualTrigger).apply {

            request = onConfigureRequest(items)

            onSuccess = {
                ModelHelper.deleteAll(items)
                doUpload()
            }

            val fnDefaultErrorHandler = onError

            onError = {
                if (retry) {
                    context.dShortToast("First Log Upload Failed.. Retrying")
                    upload(items, false)
                } else {
                    fnDefaultErrorHandler(it)
                }
            }

        }.execute()
    }

}