package omnitech.com.lgevents.transport

import com.omnitech.lgprotobuf.*
import khronos.toDate
import khronos.toString
import omnitech.com.lgevents.model.*
import omnitech.com.lgevents.utils.doSafely
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by user on 6/11/2017.
 */
val LG_DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm"
val LG_TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm:ss"

fun Date.toLgFormat(): String = toString(LG_DEFAULT_DATE_FORMAT)

fun String.toDate(format: String = LG_DEFAULT_DATE_FORMAT, default: Date = Date(0)): Date {
    doSafely {
        return SimpleDateFormat(format, Locale.US).parse(this)
    }
    return default
}

fun MsgProductCategory.toProductCategory(): ProductCategory {
    return ProductCategory().also {
        it.uuid = uuid
        it.name = name
        it.description = desc
    }
}

fun MsgProduct.toProduct(pc: ProductCategory): Product {
    return Product().also {
        it.uuid = uuid
        it.name = name
        it.description = desc
        it.imageURl = image_url
        it.price = selling_price
        it.wholeSalePrice = buying_price
        it.savingCalculatorId = saving_calculator_id
        it.category = pc
        it.videoUrl = video_url
    }
}

fun MsgPromotion.toPromotion(): Promotion {
    return Promotion().also {
        it.price = selling_price.toDouble()
        it.buyingPrice = buying_price.toDouble()
        it.name = name
        it.description = desc
    }
}

fun MsgFaqCategory.toFAQCategory(): FAQCategory {
    return FAQCategory().also {
        it.uuid = uuid
        it.name = name
        it.description = description
    }
}

fun MsgFaq.toFAQ(faqCategory: FAQCategory): FAQ {
    return FAQ().also {
        it.uuid = uuid
        it.question = question
        it.answer = answer
        it.catergory = faqCategory
    }
}


fun MsgEvent.toEvent(): Event {
    return Event().also {
        it.uuid = uuid
        it.title = title
        it.description = description
        it.host = host
        it.location = venue
        it.expiryDate = expiry_date.toDate(LG_DEFAULT_DATE_FORMAT)
        it.notificationDate = notification_date.toDate(LG_DEFAULT_DATE_FORMAT)
        it.startDate = start_date.toDate(LG_DEFAULT_DATE_FORMAT)
    }
}

fun MsgMediaResource.toResource(): Resource{
    return Resource().also {
        it.uuid = uuid
        it.title = title
        it.description = description
        it.resourceUrl = resourceUrl
        it.resourceType = resourceType
        it.expiryDate = expiry_date.toDate(LG_DEFAULT_DATE_FORMAT)
    }
}

fun MsgSavingCalculator.toSavingCalculator(): SavingCalculator {
    return SavingCalculator().also {
        it.uuid = uuid
        it.name = name
        it.description = description
    }
}

fun MsgCalculatorVariable.toSavingCalculatorVariable(sc: SavingCalculator): SavingCalculatorVariable {
    return SavingCalculatorVariable().also {
        it.uuid = uuid
        it.variableName = variable_name
        it.variableText = variable_text
        it.formula = formula
        it.orderOfDisplay = order_of_display
        it.type = if (type == MsgCalculatorVariable.Type.INPUT) "input" else "output"
        it.savingCalculator = sc
    }
}


fun ViewFaQCategory.toProtoMsg(): MsgViewFAQCategoryLog {
    return MsgViewFAQCategoryLog.Builder()
            .uuid(uuid)
            .username(username)
            .user_id(userId)
            .date_created(dateCreated.toLgFormat())
            .category_uuid(categoryUUID)
            .category_name(categoryName)
            .build()
}

fun ViewFaQ.toProtoMsg(): MsgViewFaQLog {
    return MsgViewFaQLog.Builder()
            .uuid(uuid)
            .username(username)
            .user_id(userId)
            .date_created(dateCreated.toLgFormat())
            .question_uuid(faqUUID)
            .question(faqQuestion)
            .category_uuid(categoryUUID)
            .category_name(categoryName)
            .build()
}

fun ViewProduct.toProtoMsg(): MsgViewProductLog {
    return MsgViewProductLog.Builder()
            .uuid(uuid)
            .username(username)
            .user_id(userId)
            .date_created(dateCreated.toLgFormat())
            .product_uuid(productUUID)
            .product_name(productName)
            .category_uuid(categoryUUID)
            .category_name(categoryName)
            .build()
}

fun ViewVideo.toProtoMsg():MsgViewVideoLog{
    return MsgViewVideoLog.Builder()
            .uuid(uuid)
            .username(username)
            .user_id(userId)
            .date_created(dateCreated.toLgFormat())
            .video_title(videoTitle)
            .video_uuid(videoUUID)
            .build()
}

fun VideoPlayback.toProtoMsg():MsgVideoPlayback{
    return MsgVideoPlayback.Builder()
            .uuid(uuid)
            .username(username)
            .user_id(userId)
            .date_created(dateCreated.toLgFormat())
            .video_uuid(videoUUID)
            .video_title(videoTitle)
            .session_id(sessionId)
            .action_event(actionEvent)
            .current_time_in_seconds("$currentTimeInSeconds")
            .event_time(eventTime)
            .build()
}


