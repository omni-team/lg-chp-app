package omnitech.com.lgevents.transport

import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Environment
import android.util.SparseArray
import at.huber.youtubeExtractor.Format
import at.huber.youtubeExtractor.VideoMeta
import at.huber.youtubeExtractor.YouTubeExtractor
import at.huber.youtubeExtractor.YtFile
import com.bumptech.glide.request.FutureTarget
import com.dewarder.akommons.Permission
import com.dewarder.akommons.downloadManager
import com.dewarder.akommons.isPermissionsGranted
import com.omnitech.lgprotobuf.ClientRequest
import com.omnitech.lgprotobuf.ServerResponse
import omnitech.com.lgevents.utils.*
import java.io.File
import java.net.URLEncoder








/**
 * Created by user on 5/20/2017.
 */

private val downloadCompleteIntentName = DownloadManager.ACTION_DOWNLOAD_COMPLETE
private val downloadCompleteIntentFilter = IntentFilter(downloadCompleteIntentName)


fun String.encodeUrl() = URLEncoder.encode(this, "UTF-8").replace("+", "%20")


fun ServerResponse.isSuccessful(): Boolean {
    return this.status == ServerResponse.STATUS.SUCCESS
}

fun clientRequest(request: ClientRequest.RequestType, userName: String, setupRequest: ClientRequest.Builder.() -> Unit = {}): ClientRequest {
    val builder = ClientRequest.Builder()
    builder.setupRequest()
    return builder.request(request).username(userName).build()
}


fun Context.preFetchImageGlide(finalImageUrl: String): FutureTarget<File?> {

    if (!this.isPermissionsGranted(Permission.INTERNET)) {
        e("Network Permission Not Granted")
    }
    this.i("Background Fetching Image [$finalImageUrl]")

    val futureTarget = GlideApp.with(this)
            .downloadOnly()
            .load(finalImageUrl)
            .submit()

    return futureTarget


}

fun isYoutubeUrl(url: String) = url.contains("youtube.com", ignoreCase = true)

fun extractBestYouTubeUrl(ctx: Context, youtubeUrl: String, onComplete: (String) -> Unit) {
    object : YouTubeExtractor(ctx) {
        public override fun onExtractionComplete(ytFiles: SparseArray<YtFile>?, vMeta: VideoMeta?) {

            var ytFile = ytFiles?.iterable()?.find { it.format.ext == "mp4" && it.format.height <= 480 }

            if (ytFile != null) {
                ctx.shortToast("Done Extracting Youtube URL")
                onComplete(ytFile.url)
            } else {
                ctx.shortToast("Failed to find suitable video from youtube")
            }

        }
    }.extract(youtubeUrl, false, false)
}


fun Context.playVideo(url: String, title: String = "Downloading Video") {
    val f = createRandomVideoFileFromUrl(url)

    if (!f.exists() && isYoutubeUrl(url)) {
        extractYoutubeURLAndDownload(url, title)
    } else {
        doSafely("Downloading Video: [$title]") {
            playVideoImpl(url, title, url)
        }
    }
}

private fun Context.extractYoutubeURLAndDownload(url: String, title: String) {
    shortToast("Extracting Youtube Video URL...")
    omnitech.com.lgevents.utils.doSafely("Extracting youtube URL") {
        extractBestYouTubeUrl(this, url) {
            doSafely("Downloading Video From Youtube") {
                playVideoImpl(url, title, it)
            }
        }
    }
}


private fun Context.playVideoImpl(url: String, title: String, newUrl: String) {
    val videoFile = createRandomVideoFileFromUrl(url)
    val uri = Uri.fromFile(videoFile)
    if (videoFile.exists()) {
        val intent = Intent(Intent.ACTION_VIEW, uri).apply {
            setDataAndType(uri, "video/mp4")
        }

        startActivity(intent)
    } else {

        if (isDownloading(uri.toString())) {
            shortToast("Video Is Already Downloading Please Wait!!")
        } else {
            downloadVideoFromUrl(newUrl, title, Environment.DIRECTORY_MOVIES, videoFile.name)
        }
    }
}


fun Context.downloadVideoFromUrl(fileUrl: String, downloadTitle: String, directoryType: String, fileName: String) {
    val uri = Uri.parse(fileUrl)
    val request = DownloadManager.Request(uri)
    with(request) {
        setTitle(downloadTitle)
        allowScanningByMediaScanner()
        setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        setDestinationInExternalPublicDir(directoryType, fileName)
        setVisibleInDownloadsUi(true)
    }
    shortToast("Queued Download.[$downloadTitle]...")
    e("Queued Download", uri.toString())
    downloadManager.enqueue(request)

}

fun Context.isDownloading(file: String): Boolean {

    val q = DownloadManager.Query()
            .setFilterByStatus(DownloadManager.STATUS_RUNNING
                    .or(DownloadManager.STATUS_PAUSED)
                    .or(DownloadManager.STATUS_PENDING))

    val cursor = downloadManager.query(q)

    val localUriIdx = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)

    cursor.use { c ->
        while (c.moveToNext()) {
            val localUr = c.getString(localUriIdx)

            if (localUr == file) return true
        }
    }
    return false
}

fun Context.downloadStatus(file: String) {
    val query = DownloadManager.Query()
            .setFilterByStatus(DownloadManager.STATUS_FAILED)
    val cursor = downloadManager.query(query)
    val localUriIdx = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)
    val columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)
    //column for reason code if the download failed or paused
    val columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON)
    cursor.use { c->
        while (c.moveToNext()){
            val localUri = c.getString(localUriIdx)
            val status = cursor.getInt(columnIndex)
            val reason = cursor.getInt(columnReason)

            var statusText = ""
            var reasonText = ""

            when (status) {
                DownloadManager.STATUS_FAILED -> {
                    statusText = "STATUS_FAILED"
                    reasonText = switchReason(reason)
                }
                DownloadManager.STATUS_PAUSED -> {
                    statusText = "STATUS_PAUSED"
                    reasonText = switchReason(reason)
                }
                DownloadManager.STATUS_PENDING -> statusText = "STATUS_PENDING"
                DownloadManager.STATUS_RUNNING -> statusText = "STATUS_RUNNING"
                DownloadManager.STATUS_SUCCESSFUL -> {
                    statusText = "STATUS_SUCCESSFUL"
                    reasonText = "Filename:\n$file"
                }
            }

            e("Queued Download Status:", "$statusText,$reasonText")

        }
    }
}

fun switchReason(reason:Int): String{
    var reasonText = ""
    when (reason) {
        DownloadManager.ERROR_CANNOT_RESUME -> reasonText = "ERROR_CANNOT_RESUME"
        DownloadManager.ERROR_DEVICE_NOT_FOUND -> reasonText = "ERROR_DEVICE_NOT_FOUND"
        DownloadManager.ERROR_FILE_ALREADY_EXISTS -> reasonText = "ERROR_FILE_ALREADY_EXISTS"
        DownloadManager.ERROR_FILE_ERROR -> reasonText = "ERROR_FILE_ERROR"
        DownloadManager.ERROR_HTTP_DATA_ERROR -> reasonText = "ERROR_HTTP_DATA_ERROR"
        DownloadManager.ERROR_INSUFFICIENT_SPACE -> reasonText = "ERROR_INSUFFICIENT_SPACE"
        DownloadManager.ERROR_TOO_MANY_REDIRECTS -> reasonText = "ERROR_TOO_MANY_REDIRECTS"
        DownloadManager.ERROR_UNHANDLED_HTTP_CODE -> reasonText = "ERROR_UNHANDLED_HTTP_CODE"
        DownloadManager.ERROR_UNKNOWN -> reasonText = "ERROR_UNKNOWN"

        DownloadManager.PAUSED_QUEUED_FOR_WIFI -> reasonText = "PAUSED_QUEUED_FOR_WIFI"
        DownloadManager.PAUSED_UNKNOWN -> reasonText = "PAUSED_UNKNOWN"
        DownloadManager.PAUSED_WAITING_FOR_NETWORK -> reasonText = "PAUSED_WAITING_FOR_NETWORK"
        DownloadManager.PAUSED_WAITING_TO_RETRY -> reasonText = "PAUSED_WAITING_TO_RETRY"
    }
    return reasonText
}


fun createRandomVideoFileFromUrl(url: String): File {
    val videoFileName = generateRandomVideoFileName(url)
    val videoFile = newVideoFile(videoFileName)
    return videoFile
}

fun newVideoFile(path: String): File {
    return newFileInPublicDirectory(Environment.DIRECTORY_MOVIES, path)
}

fun newFileInPublicDirectory(type: String, fileName: String): File {
    val baseFile = Environment.getExternalStoragePublicDirectory(type)
    return File(baseFile, fileName)

}

fun Format.debugStr(): String {
    return "$ext[video=$videoCodec][audio=$audioCodec][height=$height] "
}

fun Context.showDownloadStatus(downloadId:Long){
    val query = DownloadManager.Query()
    query.setFilterById(downloadId)
    val c = downloadManager.query(query)
    if (c.moveToFirst()) {
        val sizeIndex = c.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES)
        val downloadedIndex = c.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR)
        val size = c.getInt(sizeIndex)
        val downloaded = c.getInt(downloadedIndex)
        var progress = 0.0
        if (size != -1) progress = downloaded * 100.0 / size
        // At this point you have the progress as a percentage.
    }
}