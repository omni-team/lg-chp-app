package omnitech.com.lgevents.view

import android.os.Bundle
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import com.github.ajalt.flexadapter.FlexAdapterExtensionItem
import kotlinx.android.synthetic.main.resource_card.view.*
import kotlinx.android.synthetic.main.resources_rcyc_list.*
import omnitech.com.lgevents.R
import omnitech.com.lgevents.model.Resource
import omnitech.com.lgevents.presenter.ResourcesListPresenter
import org.apache.commons.lang3.StringUtils
import android.media.ThumbnailUtils
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import omnitech.com.lgevents.transport.createRandomVideoFileFromUrl
import omnitech.com.lgevents.utils.doSafely


class ResourcesListActivity : BaseSearchableActivity(){


    lateinit var presenter: ResourcesListPresenter
    var onItemClicked = {_:View->}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.resources_rcyc_list)
        presenter = ResourcesListPresenter(this).init()
    }
    override val recyclerView: AnimatedRecyclerView get() = rcyc_video_list

    override fun canShow(va: Any, q: String): Boolean = TODO("Should never be callsed")

    fun showVideos(resourcesList: List<Resource>) {
        val clickListener = onItemClicked
        val cards = resourcesList.map {ResourcesCardView(it,clickListener)  }
        rcyc_video_list.resetItemsWithAnim(cards)
    }

    class ResourcesCardView(val resource:Resource,val clickListener: (view:View) ->Unit = {})
        : FlexAdapterExtensionItem(R.layout.resource_card,span = 3,swipeDirs = ItemTouchHelper.DOWN){
        override fun bindItemView(itemView: View, position: Int) {
            doSafely {
                val videoFile = createRandomVideoFileFromUrl(resource.resourceUrl)
                val uri = Uri.fromFile(videoFile)
                var bitmap = createThumbnailFromPath(videoFile.absolutePath, MediaStore.Images.Thumbnails.MINI_KIND)
                if(bitmap != null){
                    bitmap = Bitmap.createScaledBitmap(bitmap,240,240,false)
                    itemView.lo_resource_thumbnail.setImageBitmap(bitmap)
                }
                itemView.txt_resource_name.text = resource.title
                itemView.txt_resource_description.text = StringUtils.abbreviate(resource.description,80)
                itemView.setOnClickListener(clickListener)
            }
        }
        fun createThumbnailFromPath(filePath: String, type: Int): Bitmap? {
            doSafely {
                return ThumbnailUtils.createVideoThumbnail(filePath, type)
            }
            return null
        }
    }


}