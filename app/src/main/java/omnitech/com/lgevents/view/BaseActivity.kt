package omnitech.com.lgevents.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem

/**
 * Created by user on 5/23/2017.
 */
open class BaseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableBackButton()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun enableBackButton() {

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
        }
    }

    fun setTitle(title: String) {
        supportActionBar?.setTitle(title)
    }

    fun goBack() {
        super.onBackPressed()
    }

    var fnOnDestroy = {}
    override fun onDestroy() {
        super.onDestroy()
        fnOnDestroy.invoke()
    }

}