/*
 * Copyright 2018 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omnitech.com.lgevents.view.exoplayer

import android.content.Context
import android.media.AudioManager
import android.net.Uri
import android.support.v4.media.AudioAttributesCompat
import android.support.v4.media.MediaDescriptionCompat
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.udojava.evalex.Expression.e
import khronos.Dates
import khronos.Dates.now
import khronos.toString
import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.R
import omnitech.com.lgevents.model.Resource
import omnitech.com.lgevents.model.VideoPlayback
import omnitech.com.lgevents.transport.LG_TIME_STAMP_FORMAT
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.toast
import java.io.File
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeUnit
import omnitech.com.lgevents.utils.nowNow
import omnitech.com.lgevents.utils.*

/**
 * Creates and manages a [com.google.android.exoplayer2.ExoPlayer] instance.
 */

data class PlayerState(var window: Int = 0,
                       var position: Long = 0,
                       var whenReady: Boolean = true)

class PlayerHolder(private val context: Context,
                   private val playerState: PlayerState,
                   private val playerView: PlayerView,
                   private val video: Resource,
                   private val videoSession:String) : AnkoLogger {
    val player: ExoPlayer

    // Create the player instance.
    init {
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val audioAttributes = AudioAttributesCompat.Builder()
                .setContentType(AudioAttributesCompat.CONTENT_TYPE_MUSIC)
                .setUsage(AudioAttributesCompat.USAGE_MEDIA)
                .build()
        player = AudioFocusWrapper(
                audioAttributes,
                audioManager,
                ExoPlayerFactory.newSimpleInstance(context, DefaultTrackSelector())
                        .also { playerView.player = it }
        )
        info { "SimpleExoPlayer created" }
    }

    companion object {
        val STATE_PLAY = "PLAY"
        val STATE_END = "END"
        val STATE_PAUSE = "PAUSE"
        val STATE_QUIT = "QUIT"
    }

    private fun buildMediaSource(path:String): MediaSource {
        val uriList = mutableListOf<MediaSource>()
        /*MediaCatalog.forEach {
            uriList.add(createExtractorMediaSource(it.mediaUri!!))
        }*/
        val extractorMediaSource = with(MediaDescriptionCompat.Builder()) {
            setDescription("MP4 loaded over HTTP")
            setMediaId("1")
                    .setMediaUri(Uri.fromFile(File(path)))
            setTitle("Short film Big Buck Bunny")
            setSubtitle("Streaming video")
            build()
        }
        uriList.add(createExtractorMediaSource(extractorMediaSource.mediaUri!!))
        return ConcatenatingMediaSource(*uriList.toTypedArray())
    }

    private fun createExtractorMediaSource(uri: Uri): MediaSource {
        return ExtractorMediaSource.Factory(
                DefaultDataSourceFactory(context, "exoplayer-learning"))
                .createMediaSource(uri)
    }

    // Prepare playback.
    fun start(path: String) {
        // Load media.
        player.prepare(buildMediaSource(path))
        // Restore state (after onResume()/onStart())
        with(playerState) {
            // Start playback when media has buffered enough
            // (whenReady is true by default).
            player.playWhenReady = whenReady
            player.seekTo(window, position)
            // Add logging.
            attachLogging(player)
        }
        info { "SimpleExoPlayer is started" }
    }

    // Stop playback and release resources, but re-use the player instance.
    fun stop() {
        with(player) {
            // Save state
            with(playerState) {
                position = currentPosition
                window = currentWindowIndex
                whenReady = playWhenReady
            }
            // Stop the player (and release it's resources). The player instance can be reused.
            stop(true)
        }
        info { "SimpleExoPlayer is stopped" }
    }

    // Destroy the player instance.
    fun release() {
        player.release() // player instance can't be used again.
        info { "SimpleExoPlayer is released" }
    }

    /**
     * For more info on ExoPlayer logging, please review this
     * [codelab](https://codelabs.developers.google.com/codelabs/exoplayer-intro/#5).
     */
    private fun attachLogging(exoPlayer: ExoPlayer) {
        // Show toasts on state changes.
        exoPlayer.addListener(object : Player.DefaultEventListener() {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    Player.STATE_ENDED -> {
//                        context.toast(R.string.msg_playback_ended)
                        persistEvent(STATE_END, TimeUnit.MILLISECONDS.toSeconds(player.currentPosition))
                    }
                    Player.STATE_READY -> when (playWhenReady) {
                        true -> {
//                            context.toast(R.string.msg_playback_started)
                            persistEvent(STATE_PLAY, TimeUnit.MILLISECONDS.toSeconds(player.currentPosition))
                        }
                        false -> {
//                            context.toast(R.string.msg_playback_paused)
                            persistEvent(STATE_PAUSE, TimeUnit.MILLISECONDS.toSeconds(player.currentPosition))
                        }
                    }
                }
            }
        })
        // Write to log on state changes.
        exoPlayer.addListener(object : Player.DefaultEventListener() {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                info { "playerStateChanged: ${getStateString(playbackState)}, $playWhenReady" }
            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                info { "playerError: $error" }
            }

            fun getStateString(state: Int): String {
                return when (state) {
                    Player.STATE_BUFFERING -> "STATE_BUFFERING"
                    Player.STATE_ENDED -> "STATE_ENDED"
                    Player.STATE_IDLE -> "STATE_IDLE"
                    Player.STATE_READY -> "STATE_READY"
                    else -> "?"
                }
            }
        })

    }

    private fun persistEvent(playEvent: String, currentPosition: Long) {
        var date  = nowNow().toString(LG_TIME_STAMP_FORMAT)
        e("Player","Saving event $date")
        Globals.lgPool.execute {
            video.let {
                val videoPlayback = VideoPlayback(videoUUID = it.uuid,
                        videoTitle = it.title,
                        sessionId = videoSession,
                        actionEvent = playEvent,
                        currentTimeInSeconds = currentPosition,
                        eventTime = nowNow().toString(LG_TIME_STAMP_FORMAT) )
                videoPlayback.dateCreated = nowNow()
                videoPlayback.saveEvent()
            }
        }
    }

}