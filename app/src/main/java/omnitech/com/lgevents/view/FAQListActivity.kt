package omnitech.com.lgevents.view

import android.os.Bundle
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import com.github.ajalt.flexadapter.FlexAdapterExtensionItem
import kotlinx.android.synthetic.main.faq_card.view.*
import kotlinx.android.synthetic.main.faq_list_activity.*
import omnitech.com.lgevents.R
import omnitech.com.lgevents.model.FAQ
import omnitech.com.lgevents.presenter.FaqListPresenter
import omnitech.com.lgevents.utils.doSafely
import omnitech.com.lgevents.utils.lgHandler
import omnitech.com.lgevents.utils.matchesWildCard
import omnitech.com.lgevents.utils.wildCardPattern

class FAQListActivity : BaseSearchableActivity() {


    var onItemExpanded = { v: FaqCardView -> }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.faq_list_activity)
        FaqListPresenter(this).init()
    }

    fun showQuestions(faqs: List<FAQ>) {

        rcyc_faq_list.enableAnimAndDisableLater()

        val expandItemListener = { it: View ->
            val lo_answer_container = it.lo_answer_container
            if (!lo_answer_container.isExpanded) {
                onItemExpanded(rcyc_faq_list.itemAt<FaqCardView>(it))
            }
            lo_answer_container.toggle()

        }

        val keepExpanded = false//faqs.size <= 5


        val cards = faqs.map { faq -> this.doSafely("Creating Card") { FaqCardView(faq, keepExpanded, expandItemListener) } }
                .filterNotNull()

        adapterItems = cards

    }

    override fun canShow(va: Any, q: String): Boolean {
        val wildCard = q.wildCardPattern()
        return va is FaqCardView &&
                (va.fc.answer.matchesWildCard(wildCard) || va.fc.question.matchesWildCard(wildCard))
    }

    override fun onBackPressed() {
        goBack()
    }

    override val recyclerView: AnimatedRecyclerView
        get() = rcyc_faq_list


}

class FaqCardView(val fc: FAQ, val keepExpanded: Boolean, val listener: (v: View) -> Unit)
    : FlexAdapterExtensionItem(R.layout.faq_card, span = 3, swipeDirs = ItemTouchHelper.DOWN) {
    override fun bindItemView(itemView: View, position: Int) {
        itemView.txt_qn_text.text = fc.question
        itemView.txt_qn_answer.text = fc.answer
        itemView.setOnClickListener(listener)
        if (keepExpanded)
            lgHandler.postDelayed({ itemView.lo_answer_container.expand() }, (400 * position).toLong())


    }

}