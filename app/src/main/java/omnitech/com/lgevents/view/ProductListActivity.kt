package omnitech.com.lgevents.view

import android.graphics.Paint
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import android.widget.TextView
import com.github.ajalt.flexadapter.FlexAdapterExtensionItem
import com.zhy.view.flowlayout.FlowLayout
import com.zhy.view.flowlayout.TagAdapter
import kotlinx.android.synthetic.main.product__group_filter_view.*
import kotlinx.android.synthetic.main.product_card.view.*
import kotlinx.android.synthetic.main.product_list_acitivity.*
import net.cachapa.expandablelayout.ExpandableLayout
import omnitech.com.lgevents.R
import omnitech.com.lgevents.model.Product
import omnitech.com.lgevents.model.ProductCategory
import omnitech.com.lgevents.presenter.ProductListPresenter
import omnitech.com.lgevents.utils.*
import org.humanizer.jvm.truncate


class ProductListActivity : BaseSearchableActivity() {


    lateinit var mPresenter: ProductListPresenter

    private val mCategory = ProductCategory().apply { name = "All" }

    val productCategories = mutableListOf<ProductCategory>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_list_acitivity)
        initRecyclerView()
        initFilterPanel()
    }

    override fun onStart() {
        super.onStart()
        mPresenter = ProductListPresenter(this).init()
    }

    private fun initRecyclerView() {
        rcyc_product_list.layoutManager = GridLayoutManager(this, 2)
        rcyc_product_list.disableAnim()
    }

    private fun initFilterPanel() {

        loExapandGroupFilter.setOnClickListener { pnlProductGroupItems.toggle() }


        loCollapseGroupFilter.setOnClickListener { collapseFilterBar() }


        with(loTagsLayout) {
            adapter = object : TagAdapter<ProductCategory>(productCategories) {
                override fun getView(parent: FlowLayout, position: Int, s: ProductCategory): View {
                    val tv = layoutInflater.inflate(R.layout.product__group_tag, loTagsLayout, false) as TextView
                    tv.text = s.name
                    return tv
                }
            }

            setOnTouchListener(object : OnSwipeTouchListener(this@ProductListActivity) {
                override fun onSwipeTop() {
                    collapseFilterBar()
                }
            })

            pnlProductGroupItems.setOnExpansionUpdateListener { _, state ->
                when (state) {
                    ExpandableLayout.State.EXPANDING -> rcyc_product_list.disableAnim()
                }
            }

        }


    }

    fun collapseFilterBar() {
        pnlProductGroupItems.collapse()
    }


    fun setProducts(products: List<Product>) {
        val clickListener = this@ProductListActivity::onItemClicked
        val productViews = products.map {
            ProductCardView(product = it, clickListener = clickListener)
        }
        rcyc_product_list.enableAnimAndDisableLater()
        rcyc_product_list.resetItems(productViews)
        rcyc_product_list.scrollToPosition(0)
    }

    fun setCategories(categories: List<ProductCategory>) {
        productCategories.clear()
        productCategories.add(mCategory)
        productCategories.addAll(categories)
        loTagsLayout.adapter.notifyDataChanged()
    }


    fun onItemClicked(v: View) {
        val item = rcyc_product_list.itemAt<ProductCardView>(v)
        mPresenter.onItemSelected(item.product.id)
    }

    override fun canShow(va: Any, q: String): Boolean = TODO("not implemented")

    override val recyclerView: AnimatedRecyclerView get() = rcyc_product_list


}

class ProductCardView(val product: Product, val clickListener: (view: View) -> Unit = {})
    : FlexAdapterExtensionItem(R.layout.product_card, span = 3, swipeDirs = ItemTouchHelper.DOWN) {

    override fun bindItemView(itemView: View, position: Int) {

        val isOnPromotion = product.promotion != null
        val productPrice = if (isOnPromotion) product.promotion!!.price.format() else product.price.format()


        with(itemView) {

            txtProductName.text = product.name
            txtDescripiton.text = product.description?.truncate(100)
            txtPrice.text = "UGX $productPrice"

            if (isOnPromotion) {
                imgNewRelease.visibility = View.VISIBLE

                txtPreviousPrice.visibility = View.VISIBLE
                txtPreviousPrice.text = "UGX ${product.price.format()}"
                txtPreviousPrice.paintFlags = txtPreviousPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

            } else {
                imgNewRelease.visibility = View.GONE
                txtPreviousPrice.visibility = View.GONE
            }

            v("Loading Image [${product.name}] ${product.imageURl}")
            doSafely("Loading Image ${product.imageURl}") {
                imgProduct.loadImageGlide(product.imageURl, R.drawable.placeholder_300x200)
            }

            setOnClickListener(clickListener)
        }

    }
}

