package omnitech.com.lgevents.view


import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View
import com.github.ajalt.flexadapter.FlexAdapter
import com.github.ajalt.flexadapter.FlexAdapterItem

/**
 * @author Leo on 2015/09/03
 */
class AnimatedRecyclerView @JvmOverloads constructor(context: Context,
                                                     attrs: AttributeSet? = null,
                                                     defStyle: Int = 0) : RecyclerView(context, attrs, defStyle) {

    init {
        adapter = FlexAdapter<Any>()
        layoutManager = LinearLayoutManager(context)
        //https://stackoverflow.com/questions/26856487/recyclerview-painfully-slow-to-load-cached-images-form-picasso
        //setItemViewCacheSize(10)
        //setChildrenDrawingCacheEnabled(true)
        //drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH

    }

    private var animationEnabled: Boolean = false


    fun disableAnim() {
        animationEnabled = false
    }

    fun enableAnim() {
        animationEnabled = true
    }


    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        if (animationEnabled) {
            for (i in 0..childCount - 1) {
                animate(getChildAt(i), i)
            }
        }
    }


    private fun animate(view: View, pos: Int) {

        if (!animationEnabled) return

        with(view) {
            animate().cancel()
            translationY = 100f
            alpha = 0f
            animate().alpha(1.0f)
                    .translationY(0f)
                    .setDuration(300)
                    .startDelay = (pos * 100).toLong()
        }

    }

    fun <T : Any> itemAt(v: View): T {
        val position = getChildLayoutPosition(v)
        return flexAdapter<T>().items[position]
    }


    fun <T : RecyclerView.ViewHolder> resetItems(vararg adapterItems: FlexAdapterItem<T>) {
        flexAdapter<Any>().resetItems(adapterItems.toList())
    }

    fun <T : RecyclerView.ViewHolder> resetItems(adapterItems: List<FlexAdapterItem<T>>) {
        flexAdapter<Any>().resetItems(adapterItems)
    }

    fun <T : RecyclerView.ViewHolder> resetItemsWithAnim(adapterItems: List<FlexAdapterItem<T>>) {
        enableAnimAndDisableLater()
        flexAdapter<Any>().resetItems(adapterItems)
    }

    fun <T : Any> flexAdapter(): FlexAdapter<T> = adapter as FlexAdapter<T>

    fun enableAnimAndDisableLater() {
        enableAnim()
        handler?.postDelayed({ disableAnim() }, 200)
    }
}