package omnitech.com.lgevents.view

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceActivity
import android.preference.PreferenceFragment
import android.util.AttributeSet
import omnitech.com.lgevents.R

/**
 * Created by user on 6/11/2017.
 */
class SettingsActivity : PreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentManager.beginTransaction()
                .replace(android.R.id.content, SettingsFragment())
                .commit()


    }


}

class SettingsFragment() : PreferenceFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferences);
    }

}

class LGEditTextPreference : android.preference.EditTextPreference {
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context) : super(context)

    override fun getSummary(): CharSequence {
        val summary = super.getSummary().toString()
        return compileString(summary)
    }

    fun compileString(s: String): String {
        var tmp = s
        sharedPreferences.all.forEach { it ->
            tmp = tmp.replace("{${it.key}}", "${it.value}")
        }
        return tmp
    }
}

