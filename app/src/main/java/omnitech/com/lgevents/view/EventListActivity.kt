package omnitech.com.lgevents.view

import android.os.Bundle
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import com.github.ajalt.flexadapter.FlexAdapterExtensionItem
import khronos.toString
import kotlinx.android.synthetic.main.event_card.view.*
import kotlinx.android.synthetic.main.event_list_activity.*
import omnitech.com.lgevents.R
import omnitech.com.lgevents.model.Event
import omnitech.com.lgevents.presenter.EventListPresenter
import omnitech.com.lgevents.utils.iconStr
import org.humanizer.jvm.humanize
import org.humanizer.jvm.truncate
import java.util.*

class EventListActivity : BaseActivity() {

    lateinit var presenter: EventListPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.event_list_activity)

        presenter = EventListPresenter(this).init()

    }

    fun showEvents(events: List<Event>) {
        val cards = events.map {
            EventCardView(it.startDate, it.location, it.title, it.host)
        }


        rcyc_upcoming_events.resetItemsWithAnim(cards)

        if (cards.isEmpty()) {
            rcyc_upcoming_events.visibility = View.GONE
            lo_no_events_container.visibility = View.VISIBLE
        } else {
            rcyc_upcoming_events.visibility = View.VISIBLE
            lo_no_events_container.visibility = View.GONE
        }

    }

}

class EventCardView(val eventDay: Date,
                    val eventLocation: String?,
                    val eventName: String,
                    val host: String? = null)
    : FlexAdapterExtensionItem(R.layout.event_card, span = 3, swipeDirs = ItemTouchHelper.DOWN) {

    override fun bindItemView(itemView: View, position: Int) {

        with(itemView) {

            txtEventDay.text = eventDay.toString("EEE")


            txtEventDate.text = eventDay.toString("dd/MM")

            txt_start_description.text = eventDay.humanize()

            txtEventName.text = eventName.truncate(50)

            if (!eventLocation.isNullOrEmpty()) {
                txt_event_location.text = "${iconStr(R.string.faw_compass)} $eventLocation"
                txt_event_location.visibility = View.VISIBLE
            } else {
                txt_event_location.visibility = View.GONE
            }

            //Handle the person widgets
            if (!host.isNullOrEmpty()) {
                txt_event_host.text = "${iconStr(R.string.faw_user)} $host"
                txt_event_host.visibility = View.VISIBLE
            } else {
                txt_event_host.visibility = View.GONE
            }


        }

    }
}