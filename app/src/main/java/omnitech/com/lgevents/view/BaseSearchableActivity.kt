package omnitech.com.lgevents.view

import android.graphics.Color
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import com.dewarder.akommons.adapters.SimpleSupportSearchQueryListener
import com.github.ajalt.flexadapter.FlexAdapterExtensionItem
import com.github.ajalt.flexadapter.FlexAdapterItem
import com.mikepenz.fontawesome_typeface_library.FontAwesome
import com.mikepenz.iconics.IconicsDrawable
import omnitech.com.lgevents.R

/**
 * Created by user on 7/25/2017.
 */
abstract class BaseSearchableActivity : BaseActivity() {
    var mMenu: Menu? = null

    lateinit var mSearchView: SearchView

    var onMenuCreated: (() -> Unit)? = null

    var adapterItems = listOf<FlexAdapterItem<FlexAdapterExtensionItem.ViewHolder>>()
        get() = field
        set(value) {
            field = value;
            recyclerView.resetItemsWithAnim(field)
        }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        initSearchView(menu!!)
        notifyMenuCreated(menu)
        initDefaultSearchBehavior()
        return true
    }


    fun notifyMenuCreated(menu: Menu) {
        mMenu = menu
        onMenuCreated?.invoke()
    }

    fun initSearchView(menu: Menu) {

        menuInflater.inflate(R.menu.search_menu, menu)

        val searchItem = menu.findItem(R.id.menu_search)

        searchItem.icon = IconicsDrawable(this, FontAwesome.Icon.faw_search).color(Color.WHITE).actionBar()

        mSearchView = MenuItemCompat.getActionView(searchItem) as android.support.v7.widget.SearchView

        notifyMenuCreated(menu)


    }

    fun initDefaultSearchBehavior() {
        mSearchView.setOnQueryTextListener(object : SimpleSupportSearchQueryListener() {
            override fun onQueryTextChange(newText: String): Boolean {
                onSearch(newText)
                return super.onQueryTextChange(newText)
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                onSearch(query)
                return true
            }
        })

        val searchMenuItem = mMenu!!.findItem(R.id.menu_search)

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, object : MenuItemCompat.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                onCloseSearch()
                return true
            }
        })
    }

    var onSearch = { q: String ->
        val filtered = adapterItems.filter { canShow(it, q) }
        recyclerView.resetItemsWithAnim(filtered)
    }

    var onCloseSearch = {
        recyclerView.resetItemsWithAnim(adapterItems)
    }

    abstract val recyclerView: AnimatedRecyclerView

    abstract fun canShow(va: Any, q: String): Boolean

    override fun onBackPressed() {
        if (!mSearchView.isIconified) {
            mSearchView.setQuery("", false)
            mSearchView.isIconified = true
        } else {
            super.onBackPressed()
        }
    }


}