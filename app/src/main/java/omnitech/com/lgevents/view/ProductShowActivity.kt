package omnitech.com.lgevents.view

import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import com.mikepenz.fontawesome_typeface_library.FontAwesome
import com.mikepenz.iconics.IconicsDrawable
import kotlinx.android.synthetic.main.product__saving__input_line.view.*
import kotlinx.android.synthetic.main.product__saving_card.*
import kotlinx.android.synthetic.main.product_show_activity.*
import omnitech.com.lgevents.R
import omnitech.com.lgevents.model.Product
import omnitech.com.lgevents.model.SavingCalculator
import omnitech.com.lgevents.model.SavingCalculatorVariable
import omnitech.com.lgevents.presenter.ProductShowPresenter
import omnitech.com.lgevents.utils.dp
import omnitech.com.lgevents.utils.format
import omnitech.com.lgevents.utils.loadImageGlide
import omnitech.com.lgevents.utils.toPriceText

class ProductShowActivity : BaseActivity() {

    lateinit var mPresenter: ProductShowPresenter
    private var mProduct: Product? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_show_activity)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Wholesale Price (${mProduct!!.resolveWholeSalePrice().format()}/=) Margin (${mProduct!!.resolveDifference().format()}/=)", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()

        }

        fab.setImageDrawable(IconicsDrawable(this, FontAwesome.Icon.faw_money).color(Color.WHITE).sizeDp(16))

        enableBackButton()

        mPresenter = ProductShowPresenter(this).init()
    }

    fun setProduct(p: Product) {
        mProduct = p

        if (p.isOnPromotion()) {
            txtPreviousPrice.paintFlags = txtPreviousPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            txtPreviousPrice.visibility = View.VISIBLE
            txtPreviousPrice.text = p.price.toPriceText()
            imgNewRelease.visibility = View.VISIBLE
        }

        toolbar_layout.title = p.name
        txtPrice.text = p.resolvePrice().toPriceText()
        txtDescripiton.text = p.description
        imgProduct.loadImageGlide(p.imageURl, R.drawable.image_placeholder_500x500) {
            it.override(480.dp).centerCrop()
        }
    }

    fun showVariables(sc: SavingCalculator, variables: List<SavingCalculatorVariable>) {

        txtSavingsHeader.text = sc.name
        txt_saving_description.text = sc.description
        for (it in variables) {
            appVariableWidget(it)
        }
    }


    val inputViews = mutableMapOf<SavingCalculatorVariable, View>()
    val outPutViews = mutableMapOf<SavingCalculatorVariable, View>()

    private fun appVariableWidget(variable: SavingCalculatorVariable): View {
        val view = if (variable.isInput()) {
            val v = layoutInflater.inflate(R.layout.product__saving__input_line, lo_variables_holder, false)
            v.edit_variable_input.addTextChangedListener(mPresenter)
            inputViews.put(variable, v)
            v
        } else {
            val v = layoutInflater.inflate(R.layout.product__saving_output_line, lo_variables_holder, false)
            outPutViews.put(variable, v)
            v
        }

        (view.findViewById<TextView>(R.id.txt_variable_label) as TextView).text = variable.variableText
        lo_variables_holder.addView(view)
        return view
    }


}
