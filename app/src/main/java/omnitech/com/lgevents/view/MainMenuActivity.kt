package omnitech.com.lgevents.view

import android.Manifest.permission.*
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.view.LayoutInflaterCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.dewarder.akommons.startActivity
import com.mikepenz.fontawesome_typeface_library.FontAwesome
import com.mikepenz.iconics.IconicsDrawable
import com.mikepenz.iconics.context.IconicsLayoutInflater
import omnitech.com.lgevents.R
import omnitech.com.lgevents.presenter.MainMenuPresenter


class MainMenuActivity : AppCompatActivity() {


    lateinit var menuPresenter: MainMenuPresenter
    private val REQUEST_CODE_ASK_PERMISSIONS = 123
    override fun onCreate(savedInstanceState: Bundle?) {

        LayoutInflaterCompat.setFactory(layoutInflater, IconicsLayoutInflater(delegate))

        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        menuPresenter = MainMenuPresenter(this).init()


        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar!!.setIcon(R.drawable.ic_lg_action_icon)
        try {
            if (shouldAskPermissions()) {
                ActivityCompat.requestPermissions(this, arrayOf(CAMERA, WRITE_CONTACTS, READ_CONTACTS, READ_PHONE_STATE, ACCESS_FINE_LOCATION, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE), REQUEST_CODE_ASK_PERMISSIONS)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main2, menu)

        menu.findItem(R.id.menu_sync_itmes).icon = IconicsDrawable(this, FontAwesome.Icon.faw_refresh).color(Color.WHITE).actionBar()

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        when (id) {
            R.id.action_settings -> {
                startActivity<SettingsActivity>()
                return true
            }
            R.id.menu_sync_itmes -> {
                menuPresenter.syncAll()
                return true
            }
        }


        return super.onOptionsItemSelected(item)
    }

    protected fun shouldAskPermissions(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }

}


