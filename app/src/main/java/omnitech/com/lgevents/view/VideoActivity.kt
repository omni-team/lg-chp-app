package omnitech.com.lgevents.view

import android.app.Dialog
import android.app.PictureInPictureParams
import android.content.res.Configuration
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.media.MediaDescriptionCompat
import android.support.v4.media.session.MediaSessionCompat
import android.util.Log
import android.util.Rational
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector
import com.google.android.exoplayer2.ext.mediasession.TimelineQueueNavigator
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import khronos.toString
import kotlinx.android.synthetic.main.video_activity.*
import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.R
import omnitech.com.lgevents.model.Resource
import omnitech.com.lgevents.presenter.VideoActivityPresenter
import omnitech.com.lgevents.transport.LG_DEFAULT_DATE_FORMAT
import omnitech.com.lgevents.transport.LG_TIME_STAMP_FORMAT
import omnitech.com.lgevents.utils.nowNow
import omnitech.com.lgevents.view.exoplayer.MediaCatalog
import omnitech.com.lgevents.view.exoplayer.PlayerHolder
import omnitech.com.lgevents.view.exoplayer.PlayerState


class VideoActivity : BaseActivity() {

    lateinit var presenter: VideoActivityPresenter
    private val mediaSession: MediaSessionCompat by lazy { createMediaSession() }
    private val mediaSessionConnector: MediaSessionConnector by lazy {
        createMediaSessionConnector()
    }

    private val playerState by lazy { PlayerState() }
    private lateinit var playerHolder: PlayerHolder

    private var mExoPlayerFullscreen = false
    private lateinit var mFullScreenButton: FrameLayout
    private lateinit var mFullScreenIcon: ImageView
    private lateinit var mFullScreenDialog: Dialog

    private var mResumeWindow: Int = 0
    private var mResumePosition: Long = 0
    lateinit var video:Resource
    lateinit var videoPath:String
    private var videoSession:String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.video_activity)
        videoSession = "${nowNow().toString(LG_DEFAULT_DATE_FORMAT)}_${Globals.username()}"
        presenter = VideoActivityPresenter(this).init()

        volumeControlStream = AudioManager.STREAM_MUSIC
        createMediaSession()
        createPlayer()
    }

    override fun onStart() {
        super.onStart()
        initFullscreenDialog()
        initFullscreenButton()
        startPlayer()
        activateMediaSession()
    }

    override fun onStop() {
        super.onStop()
        stopPlayer()
        deactivateMediaSession()
    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
        releaseMediaSession()
        if (mFullScreenDialog != null)
            mFullScreenDialog.dismiss()
    }

    // ExoPlayer related functions.
    private fun createPlayer() {
        playerHolder = PlayerHolder(this, playerState, exoplayer_view,video,videoSession)
    }

    private fun startPlayer() {
        playerHolder.start(videoPath)
    }

    private fun stopPlayer() {
        playerHolder.stop()
    }

    private fun releasePlayer() {
        playerHolder.release()
    }

    // MediaSession related functions.
    private fun createMediaSession(): MediaSessionCompat = MediaSessionCompat(this, packageName)

    private fun createMediaSessionConnector(): MediaSessionConnector = MediaSessionConnector(mediaSession).apply {
        setQueueNavigator(object : TimelineQueueNavigator(mediaSession) {
            override fun getMediaDescription(windowIndex: Int): MediaDescriptionCompat {
                return MediaCatalog[windowIndex]
            }
        })
    }

    private fun activateMediaSession() {
        // Note: do not pass a null to the 3rd param below, it will cause a NullPointerException.
        // To pass Kotlin arguments to Java varargs, use the Kotlin spread operator `*`.
        mediaSessionConnector.setPlayer(playerHolder.player, null)
        mediaSession.isActive = true
    }

    private fun deactivateMediaSession() {
        mediaSessionConnector.setPlayer(null, null)
        mediaSession.isActive = false
    }

    private fun releaseMediaSession() {
        mediaSession.release()
    }

    // Picture in Picture related functions.
    override fun onUserLeaveHint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enterPictureInPictureMode(
                    with(PictureInPictureParams.Builder()) {
                        val width = 16
                        val height = 9
                        setAspectRatio(Rational(width, height))
                        build()
                    }
            )
        }
    }

    override fun onPictureInPictureModeChanged(isInPictureInPictureMode: Boolean,
                                               newConfig: Configuration?) {
        exoplayer_view.useController = !isInPictureInPictureMode
    }


    public override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
    }

    private fun initFullscreenDialog() {

        mFullScreenDialog = object : Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            override fun onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog()
                super.onBackPressed()
            }
        }
    }

    private fun openFullscreenDialog() {

        (exoplayer_view.parent as ViewGroup).removeView(exoplayer_view)
        mFullScreenDialog.addContentView(exoplayer_view, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_skrink))
        Log.e("ExoPlayer","Switching to fullscreen")
        continuePlaying()
        mExoPlayerFullscreen = true
        mFullScreenDialog.show()
    }


    private fun closeFullscreenDialog() {

        (exoplayer_view.parent as ViewGroup).removeView(exoplayer_view)
        findViewById<FrameLayout>(R.id.main_media_frame).addView(exoplayer_view)
        mExoPlayerFullscreen = false
        mFullScreenDialog.dismiss()
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_expand))
        continuePlaying()
    }

    private fun initFullscreenButton() {

//        val controlView = exoplayer_view.findViewById<PlayerControlView>(R.id.exo_controller)
        mFullScreenIcon = this.findViewById(R.id.exo_fullscreen_icon)
        mFullScreenButton = this.findViewById(R.id.exo_fullscreen_button)
        mFullScreenButton.setOnClickListener {
            if (!mExoPlayerFullscreen) {
                openFullscreenDialog()
            } else {
                closeFullscreenDialog()
            }
        }
    }

    private fun continuePlaying(){
        playerState.position = playerHolder.player.contentPosition
        playerState.window = playerHolder.player.currentWindowIndex
        startPlayer()
    }


}