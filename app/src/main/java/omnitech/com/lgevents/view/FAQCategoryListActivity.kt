package omnitech.com.lgevents.view

import android.os.Bundle
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import com.dewarder.akommons.startActivity
import com.github.ajalt.flexadapter.FlexAdapterExtensionItem
import kotlinx.android.synthetic.main.faq_category_card.view.*
import kotlinx.android.synthetic.main.faq_category_list_activity.*
import omnitech.com.lgevents.R
import omnitech.com.lgevents.model.FAQCategory
import omnitech.com.lgevents.presenter.FAQCategoryListPresenter

class FAQCategoryListActivity : BaseSearchableActivity() {


    lateinit var presenter: FAQCategoryListPresenter
    var onItemClicked = { _: View -> }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.faq_category_list_activity)
        presenter = FAQCategoryListPresenter(this).init()
    }


    fun showCategories(categoryList: List<FAQCategory>) {
        val clickListener = onItemClicked
        val cards = categoryList.map { FaqCategoryCardView(it, clickListener) }
        rcyc_faq_category_list.resetItemsWithAnim(cards)

    }

    private fun onItemClicked(v: View) {

    }

    override fun canShow(va: Any, q: String): Boolean = TODO("Should Never Be Called")

    override val recyclerView: AnimatedRecyclerView get() = rcyc_faq_category_list

}

class FaqCategoryCardView(val fc: FAQCategory, val clickListener: (view: View) -> Unit = {})
    : FlexAdapterExtensionItem(R.layout.faq_category_card, span = 3, swipeDirs = ItemTouchHelper.DOWN) {
    override fun bindItemView(itemView: View, position: Int) {
        itemView.txt_faq_category_label.text = fc.name.first().toUpperCase().toString()
        itemView.txt_faq_category_name.text = fc.name
        itemView.txt_faq_category_description.text = fc.description
        itemView.setOnClickListener(clickListener)
    }

}