package omnitech.com.lgevents.model

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel
import khronos.Dates
import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.database.AppDatabase
import omnitech.com.lgevents.utils.dShortToast
import java.util.*

open class LGEventLog(
        @PrimaryKey(autoincrement = true)
        var id: Long = 0,
        @Column var username: String = Globals.username(),
        @Column var userId: String = Globals.userId(),
        @Column var dateCreated: Date = Date(),
        @Column var uuid: String = UUID.randomUUID().toString()) : BaseModel() {

    fun saveEvent() {
        Globals.context.dShortToast("Saving Event ${this}")
        save()
    }

}

@Table(database = AppDatabase::class, allFields = true)
data class ViewProduct(
        @Column var productUUID: String = "",
        @Column var productName: String = "",
        @Column var categoryName: String = "",
        @Column var categoryUUID: String = "") : LGEventLog()

@Table(database = AppDatabase::class, allFields = true)
data class ViewFaQ(
        @Column var faqQuestion: String = "",
        @Column var faqUUID: String = "",
        @Column var categoryName: String = "",
        @Column var categoryUUID: String = "") : LGEventLog()

@Table(database = AppDatabase::class, allFields = true)
data class ViewFaQCategory(
        @Column var categoryName: String = "",
        @Column var categoryUUID: String = "") : LGEventLog()

@Table(database = AppDatabase::class, allFields = true)
data class ViewVideo(
        @Column var videoTitle: String = "",
        @Column var videoUUID: String = ""
) : LGEventLog()

@Table(database = AppDatabase::class, allFields = true)
data class VideoPlayback(
        @Column var videoUUID: String = "",
        @Column var videoTitle: String = "",
        @Column var sessionId: String = "",
        @Column var actionEvent: String = "",
        @Column var currentTimeInSeconds: Long = 0,
        @Column var eventTime:String = ""
) : LGEventLog()