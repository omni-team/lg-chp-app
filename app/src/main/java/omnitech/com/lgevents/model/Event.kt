package omnitech.com.lgevents.model

import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.kotlinextensions.async
import com.raizlabs.android.dbflow.kotlinextensions.list
import com.raizlabs.android.dbflow.kotlinextensions.or
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import khronos.Dates
import khronos.beginningOfDay
import khronos.endOfDay
import omnitech.com.lgevents.database.AppDatabase
import omnitech.com.lgevents.database.ModelHelper
import java.util.*

/**
 * Created by user on 5/19/2017.
 */
@Table(database = AppDatabase::class, allFields = true)
class Event : BaseModel() {

    @PrimaryKey(autoincrement = true)
    var id: Long = 0
    lateinit var uuid: String
    lateinit var title: String
    var description: String? = null
    lateinit var notificationDate: Date
    lateinit var expiryDate: Date
    lateinit var startDate: Date
    var host: String? = null
    var location: String? = null

    companion object {
        fun findAllNonExpiredEvents(callback: (List<Event>) -> Unit) {
            val now = Dates.today.beginningOfDay

            SQLite.select().from(Event::class.java)
                    .where(Event_Table.expiryDate.greaterThan(now))
                    .orderBy(Event_Table.startDate, ModelHelper.FLAG_ASCENDING)
                    .async
                    .list { _, mutableList -> callback.invoke(mutableList) }
        }

        fun deleteAll() {
            ModelHelper.deleteAll(Event::class)
        }

        fun findAllEventsComingSoon(): List<Event> {

            val now = Dates.today

            return SQLite.select().from(Event::class.java)
                    .where(
                            Event_Table.notificationDate.lessThanOrEq(now.beginningOfDay),
                            Event_Table.startDate.greaterThan(now.endOfDay)
                    )
                    .orderBy(Event_Table.notificationDate, ModelHelper.FLAG_ASCENDING)
                    .queryList()

        }

        fun findAllEventsHappeningNow(): List<Event> {

            val now = Dates.today

            return SQLite.select().from(Event::class.java)
                    .where(
                            Event_Table.startDate.lessThanOrEq(now.beginningOfDay).or(Event_Table.startDate.lessThanOrEq(now)),
                            Event_Table.expiryDate.greaterThan(now)
                    )
                    .orderBy(Event_Table.notificationDate, ModelHelper.FLAG_ASCENDING)
                    .queryList()

        }


    }
}