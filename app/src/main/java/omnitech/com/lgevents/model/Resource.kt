package omnitech.com.lgevents.model

import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.kotlinextensions.async
import com.raizlabs.android.dbflow.kotlinextensions.list
import com.raizlabs.android.dbflow.kotlinextensions.or
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import khronos.Dates
import khronos.beginningOfDay
import omnitech.com.lgevents.database.AppDatabase
import omnitech.com.lgevents.database.ModelHelper
import omnitech.com.lgevents.utils.sqlSearchQuery
import java.util.*

@Table(database = AppDatabase::class, allFields = true)
class Resource : BaseModel() {
    @PrimaryKey(autoincrement = true)
    var id: Long = 0
    lateinit var uuid: String
    lateinit var title: String
    var description: String? = null
    lateinit var resourceUrl: String
    lateinit var resourceType: String
    lateinit var createdDate: Date
    lateinit var expiryDate: Date

    companion object {
        fun findAllNonExpiredVideos(callback: (List<Resource>)->Unit){
            val now = Dates.today.beginningOfDay
            SQLite.select().from(Resource::class.java)
                    .where(Resource_Table.expiryDate.greaterThan(now))
                    .orderBy(Resource_Table.createdDate,ModelHelper.FLAG_ASCENDING)
                    .async
                    .list{_,mutableList->callback.invoke(mutableList)}
        }

        fun deleteAll(){
            ModelHelper.deleteAll(Resource::class)
        }

        fun searchAll(q:String,callback: (List<Resource>) -> Unit){
            val query = q.sqlSearchQuery()
            val properties = Resource_Table.ALL_COLUMN_PROPERTIES.map { it.withTable() }.toTypedArray()
            SQLite.select(*properties)
                    .distinct()
                    .from(Resource::class.java)
                    .where(
                            Resource_Table.title.like(query)
                                    .or(Resource_Table.description.like(query)))
                    .orderBy(Resource_Table.title.asc()).async
                    .list{_,data->callback(data)}

        }

        fun get(id: Long, callback: (Resource?) -> Unit) {
            ModelHelper.findOneAsync(Resource::class, Resource_Table.id.eq(id), callback)
        }

        fun get(id: Long) = ModelHelper.findOne(Resource::class, Resource_Table.id.eq(id))
    }
}