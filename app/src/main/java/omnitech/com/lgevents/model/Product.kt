package omnitech.com.lgevents.model

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.ForeignKey
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.kotlinextensions.and
import com.raizlabs.android.dbflow.kotlinextensions.async
import com.raizlabs.android.dbflow.kotlinextensions.list
import com.raizlabs.android.dbflow.kotlinextensions.or
import com.raizlabs.android.dbflow.sql.language.OrderBy
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import omnitech.com.lgevents.database.AppDatabase
import omnitech.com.lgevents.database.ModelHelper
import omnitech.com.lgevents.transport.createRandomVideoFileFromUrl
import omnitech.com.lgevents.utils.sqlSearchQuery

/**
 * Created by user on 5/20/2017.
 */
@Table(database = AppDatabase::class, allFields = true)
class Product() : BaseModel() {


    @PrimaryKey(autoincrement = true)
    var id: Long = 0
    lateinit var name: String
    lateinit var uuid: String

    @Column(name = "priceDouble")
    var price: Double = 0.0
    var wholeSalePrice = 0.0
    var imageURl: String? = null
    var description: String? = null
    var packagingSize: String? = null
    var savingCalculatorId: String? = null
    var videoUrl: String? = null

    @ForeignKey
    var category: ProductCategory? = null

    @ForeignKey
    var promotion: Promotion? = null

    fun resolvePrice() = if (isOnPromotion()) promotion!!.price else price
    fun resolveWholeSalePrice() = if (isOnPromotion()) promotion!!.buyingPrice else wholeSalePrice
    fun resolveDifference() = resolvePrice() - resolveWholeSalePrice()


    fun isOnPromotion() = promotion != null

    fun getVideoUrlOrDefault(): String? {
        //return (videoUrl ?: "https://www.youtube.com/watch?v=QwMXWFD6v3g").takeIf { randomOk() }//BIG
        //return (videoUrl ?: "https://www.youtube.com/watch?v=tPEE9ZwTmy0")//.takeIf { randomOk() }//SMALL
        return videoUrl
    }

    fun videoFileExists(): Boolean {
        val url = getVideoUrlOrDefault()
        if (url == null) {
            return false
        } else {
            return createRandomVideoFileFromUrl(url).exists()

        }
    }

    companion object {

        fun findAll(callBack: (List<Product>) -> Unit) {
            ModelHelper.findAllAsync(Product::class,
                                     orderBys = listOf(
                                             OrderBy.fromProperty(Product_Table.promotion_id).descending(),
                                             OrderBy.fromProperty(Product_Table.category_id).ascending(),
                                             OrderBy.fromProperty(Product_Table.name).ascending()),
                                     callBack = callBack)
        }

        fun searchAll(q: String, categoryId: Long = -1, callBack: (List<Product>) -> Unit) {
            val sqlSearchQuery = q.sqlSearchQuery()
            var condition = Product_Table.name.like(sqlSearchQuery)
                    .or(Product_Table.description.like(sqlSearchQuery))

            if (categoryId != -1L) {
                condition = Product_Table.category_id.eq(categoryId).and(condition)
            }

            SQLite.select()
                    .from(Product::class.java)
                    .where(condition)
                    .orderByAll(
                            listOf(
                                    OrderBy.fromProperty(Product_Table.promotion_id).descending(),
                                    OrderBy.fromProperty(Product_Table.category_id).descending(),
                                    OrderBy.fromProperty(Product_Table.name).ascending()
                            )
                    ).async
                    .list { _, list -> callBack.invoke(list) }

        }

        fun deleteAll() {
            ModelHelper.deleteAll(Product::class)
        }

        fun findAllByCategory(c: ProductCategory, callBack: (List<Product>) -> Unit) {
            SQLite.select()
                    .from(Product::class.java)
                    .where(Product_Table.category_id.eq(c.id))
                    .orderByAll(
                            listOf<OrderBy>(
                                    OrderBy.fromProperty(Product_Table.promotion_id).descending(),
                                    OrderBy.fromProperty(Product_Table.name).ascending()
                            ))
                    .async.list { _, list -> callBack(list) }
        }

        fun get(id: Long, callback: (Product?) -> Unit) {
            ModelHelper.findOneAsync(Product::class, Product_Table.id.eq(id), callback)
        }

        fun get(id: Long) = ModelHelper.findOne(Product::class, Product_Table.id.eq(id))

    }


}
