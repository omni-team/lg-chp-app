package omnitech.com.lgevents.model

import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.kotlinextensions.async
import com.raizlabs.android.dbflow.kotlinextensions.list
import com.raizlabs.android.dbflow.sql.language.SQLite
import omnitech.com.lgevents.database.AppDatabase
import omnitech.com.lgevents.database.ModelHelper

/**
 * Created by user on 6/28/2017.
 */
@Table(database = AppDatabase::class, allFields = true)
class SavingCalculator {

    @PrimaryKey(autoincrement = true)
    var id: Long = 0
    lateinit var uuid: String
    lateinit var name: String
    var description: String? = null

    fun getVariables(callback: (List<SavingCalculatorVariable>) -> Unit) {
        SQLite.select()
                .from(SavingCalculatorVariable::class.java)
                .where(SavingCalculatorVariable_Table.savingCalculator_id.eq(id))
                .orderBy(SavingCalculatorVariable_Table.orderOfDisplay, ModelHelper.FLAG_ASCENDING)
                .async
                .list { _, data -> callback(data) }

    }

    companion object {
        fun findByUUID(uuid: String, callback: (SavingCalculator?) -> Unit) {
            ModelHelper.findOneAsync(SavingCalculator::class,
                                     SavingCalculator_Table.uuid.eq(uuid),
                                     callback)
        }

        fun deleteAll() {
            ModelHelper.deleteAll(SavingCalculator::class)
        }
    }
}

