package omnitech.com.lgevents.model

import com.raizlabs.android.dbflow.annotation.ForeignKey
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.kotlinextensions.async
import com.raizlabs.android.dbflow.kotlinextensions.list
import com.raizlabs.android.dbflow.kotlinextensions.or
import com.raizlabs.android.dbflow.sql.language.OperatorGroup
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import omnitech.com.lgevents.database.AppDatabase
import omnitech.com.lgevents.database.ModelHelper
import omnitech.com.lgevents.utils.sqlSearchQuery

/**
 * Created by user on 6/14/2017.
 */
@Table(database = AppDatabase::class, allFields = true)
class FAQ : BaseModel() {
    @PrimaryKey(autoincrement = true)
    var id: Long = 0L
    lateinit var question: String
    lateinit var answer: String
    lateinit var uuid: String

    @ForeignKey
    var catergory: FAQCategory? = null

    companion object {

        fun findAllByCategoryId(categoryId: Long, q: String?, callback: (List<FAQ>) -> Unit) {
            val condition = OperatorGroup.clause().and(FAQ_Table.catergory_id.eq(categoryId))
            if (!q.isNullOrBlank()) {
                val query = q!!.sqlSearchQuery()
                condition.and(
                        FAQ_Table.answer.like(query)
                                .or(FAQ_Table.question.like(query)))
            }
            SQLite.select()
                    .from(FAQ::class.java)
                    .where(condition)
                    .orderBy(FAQ_Table.question, ModelHelper.FLAG_ASCENDING)
                    .async
                    .list { _, mutableList -> callback(mutableList) }
        }
    }
}