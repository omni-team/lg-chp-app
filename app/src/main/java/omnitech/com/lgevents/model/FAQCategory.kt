package omnitech.com.lgevents.model

import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.kotlinextensions.async
import com.raizlabs.android.dbflow.kotlinextensions.list
import com.raizlabs.android.dbflow.kotlinextensions.or
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import omnitech.com.lgevents.database.AppDatabase
import omnitech.com.lgevents.database.ModelHelper
import omnitech.com.lgevents.utils.sqlSearchQuery

/**
 * Created by user on 6/14/2017.
 */
@Table(database = AppDatabase::class, allFields = true)
class FAQCategory : BaseModel() {
    @PrimaryKey(autoincrement = true)
    var id: Long = 0L
    lateinit var uuid: String
    lateinit var name: String
    lateinit var description: String

    companion object {
        fun findAll(callback: (List<FAQCategory>) -> Unit) {
            ModelHelper.findAllAsync(FAQCategory::class, callBack = callback, orderBys = listOf(FAQCategory_Table.name.asc()))
        }

        fun searchAll(q: String, callback: (List<FAQCategory>) -> Unit) {

            val query = q.sqlSearchQuery()

            val properties = FAQCategory_Table.ALL_COLUMN_PROPERTIES.map { it.withTable() }.toTypedArray()

            SQLite.select(*properties)
                    .distinct()
                    .from(FAQCategory::class.java)
                    .innerJoin(FAQ::class.java).on(FAQCategory_Table.id.withTable().eq(FAQ_Table.catergory_id))
                    .where(
                            FAQ_Table.question.like(query)
                                    .or(FAQ_Table.answer.like(query))
                                    .or(FAQCategory_Table.name.like(query))
                                    .or(FAQCategory_Table.description.like(query)))
                    .orderBy(FAQCategory_Table.name.asc()).async
                    .list { _, data -> callback(data) }
        }
    }
}