package omnitech.com.lgevents.model

import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import omnitech.com.lgevents.database.AppDatabase
import omnitech.com.lgevents.database.ModelHelper

/**
 * Created by user on 5/30/2017.
 */
@Table(database = AppDatabase::class, allFields = true)
class ProductCategory {
    @PrimaryKey(autoincrement = true)
    var id: Long = -1
    lateinit var name: String
    lateinit var uuid: String
    var description: String? = null

    companion object {
        fun findAll(callBack: (List<ProductCategory>) -> Unit) {
            ModelHelper.findAllAsync(ProductCategory::class, callBack = callBack)
        }

        fun findAllByIds(ids: List<Long>, callBack: (List<ProductCategory>) -> Unit) {
            ModelHelper.findAllAsync(ProductCategory::class,
                                     condition = ProductCategory_Table.id.`in`(ids),
                                     callBack = callBack)
        }

        fun findAll(): List<ProductCategory> {
            return ModelHelper.findAll(ProductCategory::class)
        }

        fun deleteAll() {
            ModelHelper.deleteAll(ProductCategory::class)
        }

    }

}