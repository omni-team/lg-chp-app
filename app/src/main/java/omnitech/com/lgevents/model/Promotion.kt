package omnitech.com.lgevents.model

import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel
import khronos.Dates
import omnitech.com.lgevents.database.AppDatabase
import java.util.*

/**
 * Created by user on 6/2/2017.
 */
@Table(database = AppDatabase::class, allFields = true)
class Promotion : BaseModel() {
    @PrimaryKey(autoincrement = true)
    var id: Long = -1
    var price: Double = 0.0
    var buyingPrice: Double = 0.0
    var description: String? = null
    var name: String? = null
    var startDate: Date = Dates.now
    var endDate: Date = Dates.now
}