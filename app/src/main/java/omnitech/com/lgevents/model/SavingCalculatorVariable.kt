package omnitech.com.lgevents.model

import com.raizlabs.android.dbflow.annotation.ForeignKey
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import omnitech.com.lgevents.database.AppDatabase
import omnitech.com.lgevents.database.ModelHelper

/**
 * Created by user on 6/28/2017.
 */
@Table(database = AppDatabase::class, allFields = true)
class SavingCalculatorVariable {
    @PrimaryKey(autoincrement = true)
    var id: Long = 0
    lateinit var uuid: String
    lateinit var variableName: String
    lateinit var variableText: String
    lateinit var type: String
    var formula: String? = null

    var orderOfDisplay: Int = 0

    @ForeignKey
    var savingCalculator: SavingCalculator? = null

    fun isOutput() = type == "output"
    fun isInput() = type == "input"

    companion object {

        fun deleteAll() {
            ModelHelper.deleteAll(SavingCalculatorVariable::class)
        }
    }


}