package omnitech.com.lgevents;

import android.content.Context
import com.dewarder.akommons.defaultSharedPreferences
import com.evernote.android.job.JobManager
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class Globals {
    companion object {

        lateinit var context: Context

        lateinit var jobManager: JobManager

        var httpClient = okhttp3.OkHttpClient()

        val lgPool = Executors.newFixedThreadPool(2)!!

        fun username(): String = Globals.context.defaultSharedPreferences.getString(KEY_USERNAME, "n/a")

        fun userId(): String = Globals.context.defaultSharedPreferences.getString(KEY_USERID, "n/a")


        val KEY_PHONE_NUMBER = "chp_username"
        val KEY_USERNAME = "username"
        val KEY_HUMAN_NAME = "human_name"
        val KEY_USERID = "userId"

        val KEY_SETTING_LAST_SYNC_ID = "last_sync_time"
        val SYNC_MANUAL_PAUSE_TIME = 30L//Amount of time a manual synce must wait before another starts
        val SYNC_SLEEP_UNIT = TimeUnit.SECONDS//Units used
        val SYNC_BACKGROUND_FORCE_PAUSE_PERIOD = SYNC_SLEEP_UNIT.convert(4, TimeUnit.HOURS)

        /**
         * AMOUNT OF TIME(PERIOD) BETWEEN THE JOBS. IF U CHANGE THE VALUES HERE REMEMBER
         * TO CHANGE THE JOB VERSIONS AND CANCEL THE OLD VERSIONS
         */
        val JOB_PAUSE_PERIOD_SYNC = TimeUnit.HOURS.toMillis(2)
        val JOB_PAUSE_PERIOD_EVENTS = TimeUnit.HOURS.toMillis(1)


    }
}
