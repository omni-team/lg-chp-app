package omnitech.com.lgevents

import android.app.Application
import com.evernote.android.job.JobManager
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager
import omnitech.com.lgevents.job.LgJobCreator
import omnitech.com.lgevents.transport.GlideApp


/**
 * Created by user on 5/19/2017.
 */
class CHPApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        Globals.context = this

        /*DATABASE CONFIGS*/
        FlowManager.init(FlowConfig.Builder(this).build())
        //FlowLog.setMinimumLoggingLevel(FlowLog.Level.D)

        /*JOB MANAGEMENT*/
        Globals.jobManager = JobManager.create(this).also {
            it.addJobCreator(LgJobCreator())
        }
        LgJobCreator.scheduleJobs()
        LgJobCreator.runSync()
    }

    override fun onTerminate() {
        super.onTerminate()
        FlowManager.destroy()
        GlideApp.tearDown()
        Globals.lgPool.shutdown()
    }
}