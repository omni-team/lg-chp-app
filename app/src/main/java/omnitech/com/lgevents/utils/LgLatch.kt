package omnitech.com.lgevents.utils

import java.util.concurrent.CountDownLatch

interface Latch {
    fun countDown()

    fun await()

    companion object {
        val DEFAULT = NoImpLatch()
    }
}

class NoImpLatch : Latch {
    override fun await() {}

    override fun countDown() {}
}

class LgCountDownLatch(val count: Int) : CountDownLatch(count), Latch {

    private var decr = count

    override fun countDown() {
        decr--
        d("*********************************Count DOWN $decr*************************************")
        super.countDown()
    }
}