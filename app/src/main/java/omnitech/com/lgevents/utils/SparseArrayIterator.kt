package omnitech.com.lgevents.utils

import android.util.SparseArray

class SparseArrayIterator<T : Any?>(val sa: SparseArray<T>) : Iterator<T> {
    var num: Int = 0
    override fun hasNext(): Boolean = num < sa.size()
    override fun next(): T = sa.valueAt(num++)
}

operator fun <T : Any?> SparseArray<T>.iterator() = SparseArrayIterator(this)
fun <T : Any?> SparseArray<T>.iterable(): Iterable<T> = Iterable { SparseArrayIterator(this) }
