package omnitech.com.lgevents.utils

import java.security.MessageDigest
import java.text.DecimalFormat
import java.util.*

/**
 * Created by user on 5/18/2017.
 */

fun randomOk(): Boolean = (Math.random() * 100).toInt().rem(2) != 0

fun Date.toCalendar(): GregorianCalendar {
    val cal = GregorianCalendar()
    cal.time = this
    return cal
}

fun <T> List<T>.getRandom(): T {
    val randomIndx = (java.lang.Math.random() * this.size)
    return this[randomIndx.toInt()]
}

fun <T> MutableList<T>.addIfAbsent(t: T) {
    if (!this.contains(t)) {
        this.add(t)
    }
}

fun random(lower: Int, upper: Int): Double {
    return (Math.random() * (upper - lower)) + lower
}

fun Double.toPriceText(): String {
    return "UGX ${this.format()}"
}

private val defaultPattern = "###,###"
private val defaultFormatter = DecimalFormat(defaultPattern);
fun Double.format(pattern: String = defaultPattern): String {
    val result = if (pattern === defaultPattern)
        defaultFormatter.format(this)
    else
        DecimalFormat(pattern).format(this)
    return result

}

fun String.sqlSearchQuery(): String {
    return "%${replace("%", "\\%").replace("\\s+".toRegex(), "%")}%"
}


fun String.wildCardPattern(): String {
    return "*${trim().replace("\\s+", " ").replace(" ", "*")}*"

}

fun String.md5(): String {
    return digest("MD5")
}

fun String.digest(encoding: String): String {
    val encoder = MessageDigest.getInstance(encoding)
    val digest = encoder.digest(this.toByteArray(Charsets.UTF_8))
    return LgUtils.bytesToHex(digest)
}


/**
 * Might not find *.doc or doc*
 */
fun String.matchesWildCard(pattern: String, matchCase: Boolean = false): Boolean {
    var _pattern = if (matchCase) pattern else pattern.toLowerCase()
    var text = if (matchCase) this else this.toLowerCase()
    // Match the rest of the tokens in the pattern here one by one in there specific order
    val tokens = _pattern.split("*")

    for (token in tokens) {

        val idx = text.indexOf(token)

        if (idx == -1) {
            return false
        }
        // remove all the text behind token then continue checking
        text = text.substring(idx + token.length)
    }
    return true
}

fun generateUniqueStringFromSting(prefix: String = "", url: String, postfix: String): String {
    return prefix + url.md5() + postfix

}

fun generateRandomVideoFileName(url: String): String {
    return generateUniqueStringFromSting("LG_", url, ".mp4")

}