package omnitech.com.lgevents.utils

import android.content.Context
import com.bumptech.glide.request.RequestFutureTarget
import java.util.*
import java.util.concurrent.Future

/**
 * Created by user on 6/29/2017.
 */
class FutureIsDoneNotifier<T : Any?, out F : Future<T>>(val taskDescription: String,
                                                        val ctx: Context,
                                                        val futures: List<F>,
                                                        val waitTime: Long = 10000) : Runnable {
    override fun run() {
        notifyWhenDone()
    }

    fun notifyWhenDone() {

        val copyFutures = ArrayList(futures)
        val completed = mutableListOf<F>()

        while (!copyFutures.isEmpty()) {
            for (it in futures) {
                if (it.isDone || it.isCancelled || isFutureTargetAndLoadFailed(it)) {
                    copyFutures.remove(it)
                    completed.addIfAbsent(it)
                }
            }

            ctx.dShortToast("${copyFutures.size} left of ${futures.size} ($taskDescription)")

            if (!copyFutures.isEmpty()) { //do not waste time sleeping we are done
                Thread.sleep(waitTime)
            }
        }

        ctx.dToast("All Tasks Done: [$taskDescription]")
    }

    private fun isFutureTargetAndLoadFailed(t: Future<T>): Boolean {
        return if (t is RequestFutureTarget) {
            ReflectionUtils.getValue(t, RequestFutureTarget::class.java, true, "loadFailed") == true
        } else {
            false
        }
    }

}