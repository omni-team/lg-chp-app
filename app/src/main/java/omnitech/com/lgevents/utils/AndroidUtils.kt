package omnitech.com.lgevents.utils

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Handler
import android.os.Looper
import android.support.annotation.StringRes
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.dewarder.akommons.Permission
import com.dewarder.akommons.defaultSharedPreferences
import com.dewarder.akommons.inputMethodManager
import com.dewarder.akommons.isPermissionsGranted
import omnitech.com.lgevents.transport.GlideApp
import omnitech.com.lgevents.transport.GlideRequest
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


/**
 * Created by kay
 */
fun Context.inLandScape(): Boolean = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE

fun Context.inPortrait(): Boolean = resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT

fun Context.isDevMode(): Boolean = defaultSharedPreferences.getBoolean("show_cache_indicators", false)

fun Activity.hideKeyBoard() {
    val currentFocus = this.currentFocus
    currentFocus?.let {
        inputMethodManager.hideSoftInputFromInputMethod(it.windowToken, 0)
    }
}

val Int.dp: Int
    get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), Resources.getSystem().displayMetrics).toInt()


fun ImageView.loadImageGlide(finalImageUrl: String?, placeHolder: Int, requestConfigurer: ((GlideRequest<Drawable>) -> Unit)? = null) {

    if (!this.context.isPermissionsGranted(Permission.INTERNET)) {
        e("Network Permission Not Granted")
        this.setImageResource(placeHolder)
        return
    }


    val glideRequest = GlideApp.with(this)
            .load(finalImageUrl)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(placeHolder)

    if (requestConfigurer != null) {
        requestConfigurer(glideRequest)
    } else {
        glideRequest.override(300).centerInside()
    }
    glideRequest.into(this)


}

inline fun doSafely(action: String = "", job: () -> Unit) {
    try {
        job()
    } catch (x: Exception) {
        e("SAFE-BLOCK", "!!!Error While [$action] :Reason -> ${x.message}")
        x.printStackTrace()
    }
}

inline fun <T> Context.doSafely(action: String, job: () -> T): T? {
    try {
        return job()
    } catch (x: Exception) {
        this.longToast("Error Occurred While $action: Reason -> ${x.message}")
        x.printStackTrace()
    }
    return null
}

inline fun <T> Context.doSafelyDebug(action: String, job: () -> T): T? {
    try {
        return job()
    } catch (x: Exception) {
        if (this.isDevMode()) {
            this.longToast("Error Occurred While $action: Reason -> ${x.message}")
        }
        e("Error Occurred While $action: Reason -> $x[${x.message}]")
        x.printStackTrace()
    }
    return null
}

val lgHandler = Handler(Looper.getMainLooper())
fun Context.longToast(msg: String) {
    lgHandler.post { Toast.makeText(this@longToast, msg, Toast.LENGTH_LONG).show() }
}

fun Context.shortToast(msg: String) {
    lgHandler.post { Toast.makeText(this@shortToast, msg, Toast.LENGTH_SHORT).show() }
}

fun Context.dToast(msg: String) {
    if (isDevMode()) {
        lgHandler.post { Toast.makeText(this@dToast, msg, Toast.LENGTH_LONG).show() }
    }
    this.d(msg)
}

fun Context.dShortToast(msg: String) {
    if (isDevMode()) {
        lgHandler.post { Toast.makeText(this@dShortToast, msg, Toast.LENGTH_SHORT).show() }
    }
    this.d(msg)
}

fun View.str(@StringRes resorceId: Int) = this.resources.getString(resorceId)
fun Context.str(@StringRes resorceId: Int) = this.resources.getString(resorceId)
/**
 * Retrieve a string from the resouces string wrapped in {..}
 */
fun View.iconStr(@StringRes resorceId: Int) = "{${this.resources.getString(resorceId)}}"

fun Context.iconStr(@StringRes resorceId: Int) = "{${this.resources.getString(resorceId)}}"

//LOGGING

fun Any.v(msg: () -> String) {
    if (Log.isLoggable(tag, Log.VERBOSE)) v(msg())
}

fun Any.d(msg: () -> String) {
    if (Log.isLoggable(tag, Log.DEBUG)) d(msg())
}

fun Any.i(msg: () -> String) {
    if (Log.isLoggable(tag, Log.INFO)) i(msg())
}

fun Any.e(msg: () -> String) {
    if (Log.isLoggable(tag, Log.ERROR)) e(msg())
}

fun Any.wtf(msg: () -> String) = w(msg())

fun Any.v(msg: String) = v(tag, msg)

fun Any.d(msg: String) = d(tag, msg)

fun Any.i(msg: String) = i(tag, msg)

fun Any.w(msg: String) = w(tag, msg)

fun Any.e(msg: String) = e(tag, msg)

fun Any.wtf(msg: String) = wtf(tag, msg)

fun v(tag: String, msg: String) = Log.v(tag, msg)

fun d(tag: String, msg: String) = Log.d(tag, msg)

fun i(tag: String, msg: String) = Log.i(tag, msg)

fun w(tag: String, msg: String) = Log.w(tag, msg)

fun e(tag: String, msg: String) = Log.e(tag, msg)

fun wtf(tag: String, msg: String) = Log.wtf(tag, msg)

private val Any.tag: String
    get() = javaClass.simpleName

fun Any.nowNow():Date{
    return Date()
}