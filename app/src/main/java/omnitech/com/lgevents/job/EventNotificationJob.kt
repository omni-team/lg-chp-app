package omnitech.com.lgevents.job

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import com.dewarder.akommons.notificationManager
import omnitech.com.lgevents.R
import omnitech.com.lgevents.model.Event
import omnitech.com.lgevents.view.EventListActivity
import org.humanizer.jvm.humanize

/**
 * Created by user on 6/24/2017.
 */
class EventNotificationJob(val context: Context) {

    fun showEventsComingSoon(): EventNotificationJob {
        val dueEvents = Event.findAllEventsComingSoon()

        if (dueEvents.isEmpty()) {
            return this
        }

        val notification = if (dueEvents.size == 1) {
            buildNotificationForOne(dueEvents.first())
        } else {
            buildNotificationForMany(dueEvents)
        }
        context.notificationManager.notify(0, notification)

        return this
    }

    fun showEventsHappeningNow(): EventNotificationJob {

        val events = Event.findAllEventsHappeningNow()

        if (events.isEmpty()) {
            return this
        }

        val notification = if (events.size == 1) {
            createNotification("Event: ${events.first().title}", "Happening today")
        } else {
            createNotification("${events.size}(s) Events Happening Today", "Click here for more details")
        }

        context.notificationManager.notify(0, notification)

        return this

    }

    private fun buildNotificationForOne(e: Event): Notification {
        return createNotification("Event: ${e.startDate.humanize()}", e.title)
    }

    private fun buildNotificationForMany(e: List<Event>): Notification {
        return createNotification("${e.size}(s) Events Coming Soon",
                                  "Click Here To View The Events")
    }

    private fun createNotification(title: String, text: String): Notification {
        val pIntent = createEventListIntent()
        return NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(R.drawable.ic_lg_action_icon)
                .setContentIntent(pIntent)
                .build()
    }

    private fun createEventListIntent(): PendingIntent? {
        // prepare intent which is triggered if the
        // notification is selected
        val intent = Intent(context, EventListActivity::class.java)

        // use System.currentTimeMillis() to have a unique ID for the pending intent
        val pIntent = PendingIntent.getActivity(context, System.currentTimeMillis().toInt(), intent, 0)
        return pIntent
    }
}