package omnitech.com.lgevents.job

import com.evernote.android.job.Job
import com.evernote.android.job.JobCreator
import com.evernote.android.job.JobRequest
import omnitech.com.lgevents.Globals
import omnitech.com.lgevents.transport.Synchronizer
import omnitech.com.lgevents.utils.dShortToast
import omnitech.com.lgevents.utils.i
import omnitech.com.lgevents.utils.longToast
import java.util.concurrent.TimeUnit


/**
 * Created by user on 6/15/2017.
 */

//val executor = Executors.newSingleThreadExecutor()
//
//
//val mBgHandler: Handler = createHandler("LG Background Thread")
//
//
//fun createHandler(name: String): Handler {
//    val bgThread = HandlerThread(name)
//    bgThread.start()
//    return Handler(bgThread.looper)
//}

class LgJobCreator : JobCreator {
    override fun create(tag: String?): Job? {
        val job = when (tag) {
            SyncJob.TAG -> SyncJob()
            NotificationJob.TAG -> NotificationJob()
            else -> null
        }
        return job
    }


    companion object {
        fun scheduleJobs() {
            cancelAllOldTags()
            SyncJob.scheduleJob()
            NotificationJob.scheduleJob()

        }

        fun runSync() {
            Globals.lgPool.execute {
                Synchronizer(Globals.context, false).syncAll()
            }
        }

        private fun cancelAllOldTags() {
            Globals.jobManager.cancelAllForTag("events_job")
            Globals.jobManager.cancelAllForTag("sync_job")
        }

        fun cancelAllForTagIfMany(tag: String): Set<JobRequest> {
            val allJobRequestsForTag = Globals.jobManager.getAllJobRequestsForTag(tag)
            if (allJobRequestsForTag.size > 1) {
                Globals.context?.longToast("SyncJob: Cancelling Excess Jobs(${allJobRequestsForTag.size}) For TAG($tag)")
                Globals.jobManager.cancelAllForTag(tag)
            } else {
                Globals.context?.dShortToast("SyncJob: No Excess Jobs For TAG($tag).Found In Database")

            }
            return allJobRequestsForTag
        }


    }

}

class SyncJob : Job() {
    override fun onRunJob(params: Params?): Result {
        this.i("Starting Sync Process!!")
        Synchronizer(context, false).syncAll()
        return Result.SUCCESS
    }

    companion object {
        val TAG = "sync_job_v1"

        fun scheduleJob() {
            LgJobCreator.cancelAllForTagIfMany(NotificationJob.TAG)

            val jobsForTag = Globals.jobManager.getAllJobRequestsForTag(TAG)
            if (jobsForTag.isEmpty()) {
                JobRequest.Builder(TAG)
                        .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                        .setPeriodic(Globals.JOB_PAUSE_PERIOD_SYNC, TimeUnit.MINUTES.toMillis(5))
                        .setPersisted(true)
                        .build()
                        .schedule()
            }
        }
    }
}

class NotificationJob : Job() {
    override fun onRunJob(params: Params?): Result {
        this.i("Starting Sync Process!!")
        return try {

            EventNotificationJob(context)
                    .showEventsComingSoon()
                    .showEventsHappeningNow()
            Result.SUCCESS

        } catch (e: Exception) {
            this.i("Failed to run event notification job")
            Result.FAILURE
        }


    }


    companion object {
        val TAG = "events_job_v1"

        fun scheduleJob() {
            LgJobCreator.cancelAllForTagIfMany(TAG)

            val jobsForTag = Globals.jobManager.getAllJobRequestsForTag(TAG)
            if (jobsForTag.isEmpty()) {
                JobRequest.Builder(TAG)
                        .setPeriodic(Globals.JOB_PAUSE_PERIOD_EVENTS, TimeUnit.MINUTES.toMillis(5))
                        .setPersisted(true)
                        .build()
                        .schedule()
            }
        }
    }
}

