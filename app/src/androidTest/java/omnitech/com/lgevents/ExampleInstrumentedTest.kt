package omnitech.com.lgevents

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import khronos.*
import omnitech.com.lgevents.database.ModelHelper
import omnitech.com.lgevents.model.Event
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import java.util.concurrent.CountDownLatch

/**
 * Instrumentation test, which will execute on an Android device.

 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    @Throws(Exception::class)
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()

        assertEquals("omnitech.com.lgevents", appContext.packageName)
    }

    @Test
    fun creatingDummyEvents() {
        DummyData.createDummyEvents()
        assert(ModelHelper.findAll(Event::class).isNotEmpty())
    }

    @Test
    fun test_expired_event_does_not_return() {
        deleteAll()

        createEvent(expiry = Dates.yesterday)

        val future = Future<List<Event>>()

        Event.findAllNonExpiredEvents { future.set(it) }

        assertNotNull(future.get())

        assertEquals(1, ModelHelper.findAll(Event::class).size)

        assertEquals(0, future.get().size)

    }

    @Test
    fun test_todays_event_does_return() {
        deleteAll()

        createEvent()

        val future = Future<List<Event>>()

        Event.findAllNonExpiredEvents { future.set(it) }

        assertNotNull(future.get())

        assertEquals(1, ModelHelper.findAll(Event::class).size)

        assertEquals(1, future.get().size)

    }


    //UPCOMING EVENTS

    @Test
    fun test_upcoming_events_do_not_show_expired_events() {
        deleteAll()
        createEvent(notifiction = Dates.today - 7.days, expiry = Dates.now - 2.days)

        val events = Event.findAllEventsComingSoon()

        assertEquals(0, events.size)


    }

    @Test
    fun test_upcoming_events_does_not_return_events_for_today() {
        deleteAll()
        createEvent(notifiction = Dates.today - 7.days, start = Dates.now, expiry = Dates.tomorrow)

        val events = Event.findAllEventsComingSoon()

        assertEquals(0, events.size)
    }

    @Test
    fun test_upcoming_events_show_the_expected() {
        deleteAll()
        createEvent(notifiction = Dates.today - 7.days, start = Dates.tomorrow, expiry = Dates.tomorrow)

        val events = Event.findAllEventsComingSoon()

        assertEquals(1, events.size)
    }

    @Test
    fun test_upcoming_events_show_on_the_day_of_notification() {
        deleteAll()
        createEvent(notifiction = Dates.today.beginningOfDay, start = Dates.tomorrow, expiry = Dates.tomorrow)

        val events = Event.findAllEventsComingSoon()

        assertEquals(1, events.size)
    }

    //
    //HAPPENING EVENTS
    // event starts today ends today
    @Test
    fun test_findAllEventsHappeningNow_event_starts_today_ends_today() {
        deleteAll()
        createEvent(expiry = Dates.now + 1.hour)

        val events = Event.findAllEventsHappeningNow()

        assertEquals(1, events.size)
    }

    //event started yesterday ends tomorrow
    @Test
    fun test_findAllEventsHappeningNow_event_starts_yesterday_ends_tommorow() {
        deleteAll()
        createEvent(start = Dates.yesterday, expiry = Dates.now + 1.hour)

        val events = Event.findAllEventsHappeningNow()

        assertEquals(1, events.size)
    }

    @Test
    fun test_findAllEventsHappeningNow_event_starts_yesterday_ended_1_hour_ago() {
        deleteAll()
        createEvent(start = Dates.yesterday, expiry = Dates.now - 1.hour)

        val events = Event.findAllEventsHappeningNow()

        assertEquals(0, events.size)
    }


    private fun deleteAll() {
        ModelHelper.deleteAll(Event::class)
    }


    fun createEvent(notifiction: Date = Dates.now, start: Date = Dates.now, expiry: Date = Dates.now) {
        Event().apply {
            title = DummyData.Fakeit.book().title()
            description = DummyData.Fakeit.gameOfThrones().quote()
            host = DummyData.Fakeit.book().author().takeIf { DummyData.randomOk() }
            location = DummyData.Fakeit.ancient().titan().takeIf { DummyData.randomOk() }


            notificationDate = notifiction
            startDate = start
            expiryDate = expiry
        }.save()

    }


}

class Future<T : Any>() {

    lateinit var value: T
    private val latch = CountDownLatch(1)

    fun set(value: T) {
        this.value = value
        latch.countDown()
    }

    fun get(): T {
        latch.await()
        return value
    }

}
