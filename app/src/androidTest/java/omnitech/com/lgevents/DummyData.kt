package omnitech.com.lgevents

import com.github.javafaker.Faker
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.kotlinextensions.databaseForTable
import com.raizlabs.android.dbflow.kotlinextensions.save
import khronos.Dates
import khronos.days
import khronos.plus
import omnitech.com.lgevents.database.ModelHelper
import omnitech.com.lgevents.model.*
import omnitech.com.lgevents.utils.getRandom
import omnitech.com.lgevents.utils.random
import java.util.*

/**
 * Created by user on 5/20/2017.
 */

class DummyData {

    companion object {

        val Fakeit = Faker()

        fun createDummyProducts(): List<Product> {
            val dbCategories = ProductCategory.findAll()
            val categories = if (dbCategories.isEmpty()) MutableList(5) { createDummyCategory().apply { save() } } else dbCategories

            val dbPromotions = ModelHelper.findAll(Promotion::class)
            val promotions: MutableList<Promotion?> = if (dbPromotions.isEmpty()) MutableList<Promotion?>(3) { createDummyPromotion().apply { save() } } else dbPromotions.toMutableList()

            repeat(15) {
                promotions.add(null)
            }

            val products = mutableListOf(
                    Product().apply {
                        name = "Promotional"
                        imageURl = "http://images4.fanpop.com/image/photos/22900000/So-Cool-random-22921826-500-333.jpg"
                        description = randProdDescription()
                        price = random(10_000, 200_000)
                        wholeSalePrice = price * 0.85
                        promotion = promotions.first()
                        savingCalculatorId = createSavingsCalculator().uuid

                    },

                    Product().apply {
                        name = randProdName()
                        imageURl = randImageURL()
                        price = random(10_000, 200_000)
                        wholeSalePrice = price * 0.85

                    },
                    Product().apply {
                        name = randProdName()
                        description = randProdDescription()
                        price = random(10_000, 200_000)
                        wholeSalePrice = price * 0.85

                    }
            ).apply { repeat(5) { add(randProduct()) } }


            Collections.shuffle(products)


            products.forEach {
                it.category = categories.getRandom()
                if (it.promotion == null) {
                    it.promotion = promotions.getRandom()
                }
                it.save()
            }



            return products

        }

        private fun randProduct(): Product {
            return Product().apply {
                name = randProdName()
                imageURl = randImageURL()
                description = randProdDescription()
                price = random(10_000, 200_000)
                wholeSalePrice = price * 0.85
            }
        }

        private fun createSavingsCalculator(): SavingCalculator {
            val all = ModelHelper.findAll(SavingCalculator::class)
            if (all.isNotEmpty()) return all.first()

            val sc = SavingCalculator().apply {
                uuid = UUID.randomUUID().toString()
                name = randProdName()
                description = randProdDescription()
                save()
            }

            val variables = listOf(
                    SavingCalculatorVariable().apply {
                        uuid = UUID.randomUUID().toString()
                        variableName = "charcoal"
                        variableText = "How much charcoal do you use?"
                        orderOfDisplay = 1
                        type = "input"

                    },
                    SavingCalculatorVariable().apply {
                        uuid = UUID.randomUUID().toString()
                        variableName = "kilograms"
                        variableText = "How Kgs Do you make"
                        orderOfDisplay = 2
                        type = "input"
                    },
                    SavingCalculatorVariable().apply {
                        uuid = UUID.randomUUID().toString()
                        variableName = "perweek_saving"
                        variableText = "Saving per week"
                        orderOfDisplay = 3
                        type = "output"
                        formula = "charcoal * price * kilograms * 1"

                    },
                    SavingCalculatorVariable().apply {
                        uuid = UUID.randomUUID().toString()
                        variableName = "permonth_saving"
                        variableText = "Saving permonth"
                        orderOfDisplay = 4
                        type = "output"
                        formula = "charcoal * price * kilograms * 2"

                    },
                    SavingCalculatorVariable().apply {
                        uuid = UUID.randomUUID().toString()
                        variableName = "derived_summation"
                        variableText = "Total Derived Summation"
                        orderOfDisplay = 4
                        type = "output"
                        formula = "permonth_saving + perweek_saving"

                    }
            )

            variables.forEach {
                it.savingCalculator = sc
                it.save()
            }

            return sc

        }

        private fun Product.randImageURL() = "http://loremflickr.com/320/240/${name.split(" ")[0]}"

        private fun randProdDescription() = Fakeit.chuckNorris().fact()?.takeIf { randomOk() }

        private fun randProdName() = Fakeit.commerce().productName()

        fun randomOk(): Boolean = (Math.random() * 100).toInt().rem(2) != 0

        fun createDummyCategory() = ProductCategory().apply {
            name = Fakeit.commerce().material()
            description = Fakeit.gameOfThrones().quote()
            uuid = UUID.randomUUID().toString()
        }

        fun createDummyPromotion() = Promotion().apply {
            name = Fakeit.commerce().productName()
            startDate = Dates.now
            endDate = Dates.tomorrow
            price = 10_000.0
        }

        fun createDummyFaqCategories() {
            repeat(10) {
                println("KKKKK $it")
                FAQCategory().apply {
                    name = Fakeit.company().industry()
                    description = Fakeit.gameOfThrones().quote()
                    uuid = UUID.randomUUID().toString()
                    save()
                }
            }
        }

        fun createDummyFaqs() {
            val categories = ModelHelper.findAll(FAQCategory::class)
            val faqs = mutableListOf<FAQ>()
            repeat(100) {
                faqs.add(
                        FAQ().apply {
                            question = Fakeit.chuckNorris().fact()
                            answer = Fakeit.chuckNorris().fact()
                            uuid = UUID.randomUUID().toString()
                            catergory = categories.getRandom()
                        }
                )
            }

            FAQ_Table(databaseForTable<FAQ>()).insertAll(faqs)
        }

        fun createDummyEvents() {
            val events = mutableListOf<Event>()

            repeat(30) {
                events.add(
                        Event().apply {
                            title = Fakeit.book().title()
                            description = Fakeit.gameOfThrones().quote()
                            notificationDate = Fakeit.date().between(Dates.now, Dates.now + 70.days)
                            startDate = Fakeit.date().between(notificationDate, notificationDate + 5.days)
                            expiryDate = Fakeit.date().between(startDate, startDate.plus(5.days))
                            host = Fakeit.book().author().takeIf { randomOk() }
                            location = Fakeit.ancient().titan().takeIf { randomOk() }
                        }
                )
            }

            events.add(
                    Event().apply {
                        title = Fakeit.book().title()
                        description = Fakeit.gameOfThrones().quote()
                        notificationDate = Dates.now
                        startDate = Fakeit.date().between(notificationDate, notificationDate.plus(5.days))
                        expiryDate = Fakeit.date().between(startDate, startDate.plus(5.days))
                        host = Fakeit.book().author().takeIf { randomOk() }
                        location = Fakeit.ancient().titan().takeIf { randomOk() }
                    }
            )
            events.add(
                    Event().apply {
                        title = Fakeit.book().title()
                        description = Fakeit.gameOfThrones().quote()
                        notificationDate = Dates.now
                        startDate = Fakeit.date().between(notificationDate, notificationDate.plus(5.days))
                        expiryDate = Fakeit.date().between(startDate, startDate.plus(5.days))
                        host = Fakeit.book().author().takeIf { randomOk() }
                        location = Fakeit.ancient().titan().takeIf { randomOk() }
                    }
            )
            events.add(
                    Event().apply {
                        title = Fakeit.book().title()
                        description = Fakeit.gameOfThrones().quote()
                        notificationDate = Dates.now
                        startDate = Dates.now
                        expiryDate = Fakeit.date().between(startDate, startDate.plus(5.days))
                        host = Fakeit.book().author().takeIf { randomOk() }
                        location = Fakeit.ancient().titan().takeIf { randomOk() }
                    }
            )



            FlowManager.getModelAdapter(Event::class.java).insertAll(events)
        }
    }


}